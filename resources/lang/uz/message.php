<?php


return [
    'name_uz' => 'Nomi',
    'name_ru' => 'Название',
    'name_en' => 'Name',
    'description_uz' => 'Izoh',
    'description_ru' => 'Описание',
    'title_uz' => 'Sarlavha',
    'title_ru' => 'Заглавие',
    'title_en' => 'Title',
    'document_format' => ' fayl formati mos emas yoki fayl hajmi 50 Mb dan ortiq!',
    'Unauthorized' => 'Ruxsat etilmagan',
    'Username or password is invalid' => 'Foydalanuvchi nomi yoki parol xato!',
    'Phone number is invalid' => 'Telefon raqami xato',
    'Confirmation code sent' => 'Tasdiqlash kodi yuborildi',
    'The code sent is correct' => 'Yuborilgan kod to\'g\'ri',
    'Confirmation code is invalid' => 'Tasdiqlash kodi xato',
    'Phone number or code is invalid' => 'Telefon raqami yoki kod xato',
    'Man' => 'Erkak',
    'Women' => 'Ayol',
    'Successfully completed' => 'Muvaffaqiyatli yakunlandi',
    'Old password error' => 'Eski parol xatosi',
    'Not Found' => 'Topilmadi',
    'New faculty' => 'Yangi fakultet',
    'report has been sent' => 'Hisobot jo\'natildi',
    'Task not found' => 'Vazifa topilmadi'
];
