<?php


return [
    'name_uz' => 'Nomi',
    'name_ru' => 'Название',
    'name_en' => 'Name',
    'description_uz' => 'Izoh',
    'description_ru' => 'Описание',
    'title_uz' => 'Sarlavha',
    'title_ru' => 'Заглавие',
    'title_en' => 'Title',
    'document_format' => ' несовместимый формат файла или размер файла более 50 Мб!',
    'Unauthorized' => 'Неавторизованный',
    'Username or password is invalid' => 'Имя пользователя или пароль недействительны',
    'Phone number is invalid' => 'Номер телефона недействителен',
    'Confirmation code sent' => 'Код подтверждения отправлен',
    'The code sent is correct' => 'Присланный код правильный',
    'Confirmation code is invalid' => 'Код подтверждения недействителен',
    'Phone number or code is invalid' => 'Номер телефона или код недействителен',
        'Man' => 'Мужчина',
    'Women' => 'Женщины',
    'Successfully completed' => 'Успешно завершено',
    'Old password error' => 'Ошибка старого пароля',
    'Not Found' => 'Не обнаружена',
    'New faculty' => 'Новый факультет',
    'report has been sent' => 'Отчет отправлен',
    'Task not found' => 'Задача не найдена'
];
