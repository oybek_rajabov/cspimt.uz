@extends('layouts.app')
@section('content')
    @include('talimtili.delete')
    <!-- begin page-header -->
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-left" style="margin-bottom: 10px;">
            <li class="breadcrumb-item"><a href="/">Asosiy</a></li>
            <li class="breadcrumb-item active">Ta'lim tillari</li>
        </ol>
        @include('talimtili.modals')
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a onclick="showCreateModal()"  title="Qo'shish"
                               class="btn btn-xs btn-success">
                                <i class="fa fa-plus"></i> Qo'shish
                            </a>
                        </div>
                        <h4 class="panel-title">Ta'lim tillari</h4>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive kv-grid-container">
                            @include('talimtili._columns')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@include('talimtili.indexJs')


