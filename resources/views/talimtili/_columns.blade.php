<table class="table table-bordered kv-grid-table table-striped table-condensed">
    <thead>
    <tr>
        <th width="20">@sortablelink('id','#')</th>
        <th width="20">ID</th>
        <th>@sortablelink('name','Nomi')</th>
        <th>@sortablelink('key','Kalit')</th>
        <th>Harakatlar</th>
    </tr>
    <tr>
        @include('talimtili.search')
    </tr>
    </thead>
    <tbody>
    @foreach($datas  as $data)
        <tr>
            <td>{{ $datas->perPage()*($datas->currentPage() - 1) + $loop->iteration }}</td>
            <td>{{ $data->id }}</td>
            <td>{{$data->name}}</td>
            <td>{{$data->key}}</td>
            <td class="skip-export kv-align-center kv-align-middle crud-datatable"
                style="width:50px;" data-col-seq="11">
                {{actionModalEdit($data->id)}}
                {{actionModalDelete($data->id)}}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
{{$datas->links('pagination::bootstrap-4')}}