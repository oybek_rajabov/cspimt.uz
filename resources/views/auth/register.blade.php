@extends('layouts.auth')

@section('content')
<!-- begin register -->
<div class="register register-with-news-feed">
    <!-- begin news-feed -->
    <div class="news-feed">
        <div class="news-image">
            <img src="{{asset('assets/img/login-bg/bg-8.jpg')}}" alt="" />
        </div>
        <div class="news-caption">
            <h4 class="caption-title"><i class="fa fa-edit text-success"></i> Announcing the Color Admin app</h4>
            <p>
                As a Color Admin Apps administrator, you use the Color Admin console to manage your organization’s account, such as add new users, manage security settings, and turn on the services you want your team to access.
            </p>
        </div>
    </div>
    <!-- end news-feed -->
    <!-- begin right-content -->
    <div class="right-content">
        <!-- begin register-header -->
        <h1 class="register-header">
            Sign Up
        </h1>
        <!-- end register-header -->
        <!-- begin register-content -->
        <div class="register-content">
            <form action="{{route('register')}}" method="POST" class="margin-bottom-0">
                @csrf
                <label class="control-label">Name <span class="text-danger">*</span></label>
                <div class="row row-space-10">
                    <div class="col-md-12 m-b-15">
                        <input type="text" name='username' class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" placeholder="Username" required />
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>
                <label class="control-label">Email <span class="text-danger">*</span></label>
                <div class="row m-b-15">
                    <div class="col-md-12">
                        <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email address" value="{{old('email')}}" required />
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>
                <label class="control-label">Password <span class="text-danger">*</span></label>
                <div class="row m-b-15">
                    <div class="col-md-12">
                        <input type="password" name="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" required />
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>
                <label class="control-label">Confirm password <span class="text-danger">*</span></label>
                <div class="row m-b-15">
                    <div class="col-md-12">
                        <input type="password" name="password_confirmation" class="form-control @error('password_confirmation') is-invalid @enderror" placeholder="Confirm password" required />
                        @error('confirmPassword')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>
                <div class="register-buttons">
                    <button type="submit" class="btn btn-primary btn-block btn-lg">Sign Up</button>
                </div>
                <div class="m-t-20 m-b-40 p-b-40 text-inverse">
                    Already a member? Click <a href="{{route('login')}}"><span class="label label-theme m-l-5">here</span></a> to login.
                </div>
            </form>
        </div>
        <!-- end register-content -->
    </div>
    <!-- end right-content -->
</div>
<!-- end register -->
@endsection
