@extends('layouts.auth')

@section('content')
    <!-- begin login -->
    <div class="login bg-black animated fadeInDown">
        <!-- begin brand -->
        <div class="login-header">
            <div class="brand">
                <img @if(file_exists(public_path('images/logo.png'))) src="{{asset('images/logo.png')}}" style="max-width: 80px; width: auto; max-height: 80%"  @else src="{{asset('img/noimg.jpg')}}" style="max-width: 30px; max-height: 30px; border-radius: 50%;" @endif />
                <b style="font-weight: 500;">CSPU marketing</b>
            </div>
            <div class="icon">
                <i class="fa fa-sign-in"></i>
            </div>
        </div>
        <!-- end brand -->
        <div class="login-content">
            <form action="{{route('login')}}" method="POST" class="margin-bottom-0">
                @csrf
                <div class="form-group m-b-20">
                    <input type="text" name="username" value="{{old('username')}}" class="form-control input-lg inverse-mode no-border @if('username') is-invalid @enderror" placeholder="Username" required />
                </div>
                <div class="form-group m-b-20">
                    <input type="password" name="password" class="form-control input-lg inverse-mode no-border @if('password') is-invalid @enderror" placeholder="Parol" required />
                </div>
                <div class="login-buttons">
                    <button type="submit" class="btn btn-success btn-block btn-lg">Kirish</button>
                </div>
            </form>
        </div>
    </div>
    <!-- end login -->

@endsection
@section('loginJs')
    <script>
        $(document).ready(function (){
            $('body').removeClass('bg-white');
        });
    </script>
@endsection
