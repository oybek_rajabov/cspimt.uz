<table class="table table-bordered">
	<thead>
	<tr>
		<th style="width: 50px;">@sortablelink('id', '#')</th>
		<th>Rasm</th>
		<th>@sortablelink('fio','FIO')</th>
		<th>@sortablelink('role.name','Rol')</th>
		<th>@sortablelink('username','Login')</th>
		<th>@sortablelink('phone','Telefon')</th>
		<th style="width:130px;">Harakatlar</th>
	</tr>
	<tr>
		@include('users.search')
	</tr>
	</thead>
	<tbody>
	@foreach($users as $user)
		<tr>
			<td>{{ $users->perPage()*($users->currentPage() - 1) + $loop->iteration }}</td>
			<td><img @if($user->image) src="{{asset('storage/users/'.$user->image)}}"  @else src="{{asset('img/noimg.jpg')}}" @endif style="max-width: 60px; min-width: 60px; max-height: 60px" alt=""></td>
			<td>{{$user->fio}}</td>
			<td>{{$user->role->name}}</td>
			<td>{{$user->username}}</td>
			<td>{{$user->phone}}</td>
			<td style="display: flex;">
				{{actionView(route('users.show',$user))}}
				{{actionEdit(route('users.edit',$user))}}
				@if( $user->role->key != 'admin')
					{{actionModalDelete($user->id)}}
				@endif
			</td>
		</tr>
	@endforeach
	</tbody>
</table>
{{$users->links('pagination::bootstrap-4')}}
