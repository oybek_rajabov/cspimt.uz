<form action="#" id="actionCreateForm">
    @csrf
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="username">Login*</label>
                <input type="username" class="form-control" name="username" id="username" placeholder="Login">
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="password">Parol*</label>
                <input type="password" class="form-control" name="password" id="password" maxlength="20" minlength="6" pattern="[A-Za-z0-9]" placeholder="kamida 6 ta belgi">
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="fio">FIO*</label>
        <input type="text" class="form-control" id="fio" name="fio" placeholder="FIO">
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="role">Rol*</label>
                <select name="role" id="role" class="form-control">
                    <option value="">Tanlang..</option>
                    @foreach($roles as $role)
                        <option value="{{$role->id}}">{{$role->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <label for="phone">Telefon</label>
            <input type="text" class="form-control" id="phone" name="phone" placeholder="(998)90-999-99-99">
        </div>
    </div>
</form>
<div id="errorAlert">

</div>





