<form method="get" action="{{route('users.index')}}">
    @csrf
    <th></th>
    <th></th>
    <th><input type="text" name="fio" value="{{ request('fio') }}" class="form-control"></th>
    <th>
        <select class="form-control" name="role" id="">
            <option value="">Tanlang</option>
            @foreach($roles as $role)
                <option value="{{$role->id}}" @if(request('role') == $role->id)selected @endif>{{$role->name}}</option>
            @endforeach
        </select>
    </th>
    <th><input type="text" name="username" value="{{ request('username') }}" class="form-control"></th>
    <th><input type="text" name="phone" value="{{ request('phone') }}" class="form-control"></th>
    <th>
        <button type="submit" class="btn btn-warning btn-sm">
            <i class="fa fa-filter"></i> Qidiruv
        </button>
    </th>
</form>
