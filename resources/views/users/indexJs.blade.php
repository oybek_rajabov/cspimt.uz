
@section('customJs')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#phone').inputmask('+\\9\\9899-999-99-99');
        })


        function showAction(id) {
            var modal = $('#createModal');
            $.ajax({
                method: 'GET',
                url: 'users/' + id,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function (response) {
                    modal.find('.modal-header').html(response.header);
                    modal.find('.modal-body').html(response.content);
                    modal.find('.modal-footer').html(response.footer);
                    modal.modal('show');
                },
                error: function (response) {
                    $.gritter.add({
                        title: "Xatolik",
                        text: response.message,
                        class_name: "gritter-light",
                        image: '{{asset('assets/img/error.png')}}',
                        sticky: false,
                        time: 3000
                    });
                }
            });
        }

        function showCreateModal() {
            var modal1 = $('#createModal');
            console.log('alert')

            $.ajax({
                method: 'GET',
                url: '{{route('users.create')}}',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function (response) {
                    modal1.find('.modal-header').html(response.header);
                    modal1.find('.modal-body').html(response.content);
                    modal1.find('.modal-footer').html(response.footer);
                    modal1.modal('show');
                    $('#phone').inputmask('+\\9\\9899-999-99-99');

                },
                error: function (response) {
                    $.gritter.add({
                        title: "Xatolik",
                        text: response.message,
                        class_name: "gritter-light",
                        image: '{{asset('assets/img/error.png')}}',
                        sticky: false,
                        time: 3000
                    });
                }
            });
        }

        $(document).on('click', '#createSubmit', function (e) {
            e.preventDefault();
            var formData = $('#actionCreateForm').serialize();
            $.ajax({
                method: 'POST',
                url: '{{route('users.store')}}',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: formData,
                success: function (response) {
                    $('#createModal').modal('hide');
                    $.gritter.add({
                        title: "Muvaffaqiyatli yakunladi",
                        text: response.success,
                        class_name: "gritter-light",
                        image: '{{asset('assets/img/success.png')}}',
                        sticky: false,
                        time: 200,
                        // after_close: function () {
                        //     window.location.reload();
                        // }
                    });
                    window.location.reload();

                },
                error: function (response) {
                    var errors = JSON.parse(response.responseText).errors;
                    var errorsHtml = '<ul>';
                    $.each(errors, function (index, value) {
                        errorsHtml += '<li><p class="text-danger">' + value + '</p></li>';
                    });
                    errorsHtml += '</ul>';
                    $('#errorAlert').html(errorsHtml);

                }
            });
        })

        function showDeleteModal(id) {
            var modal = $('#deleteModal');
            $('#createModal').modal('hide');
            $('#updateModal').modal('hide');
            $.ajax({
                method: 'GET',
                url:'/admin/users/delete/'+id,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function (response){
                    modal.find('.modal-header').html(response.header);
                    modal.find('.modal-body').html(response.content);
                    modal.find('.modal-footer').html(response.footer);
                    modal.modal('show');
                },
                error: function (response) {
                    $.gritter.add({
                        title: "Xatolik",
                        text: response.message,
                        class_name: "gritter-light",
                        image: '{{asset('assets/img/error.png')}}',
                        sticky: false,
                        time: 3000
                    });
                }
            });
        }

        function deleteUser(id){
            $.ajax({
                method: 'delete',
                url: 'users/delete/'+id,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function (response) {
                    $.gritter.add({
                        title: "Muvaffaqiyatli yakunladi",
                        text: response.success,
                        class_name: "gritter-light",
                        image: '{{asset('assets/img/success.png')}}',
                        sticky: false,
                        time: 200,
                    });
                    window.location.reload();

                },
                error: function (response){
                    $.gritter.add({
                        title: "Xatolik",
                        text: response.message,
                        class_name: "gritter-light",
                        image: '{{asset('assets/img/error.png')}}',
                        sticky: false,
                        time: 200
                    });

                }
            });

        }
    </script>
@endsection
