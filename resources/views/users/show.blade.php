@extends('layouts.app')
@section('content')
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-left" style="margin-bottom: 10px;">
            <li class="breadcrumb-item"><a href="/">Asosiy</a></li>
            <li class="breadcrumb-item"><a href="{{route('users.index')}}">Foydalanuvchilar</a></li>
            <li class="breadcrumb-item active">Foydalanuvchi</li>
        </ol>
        <!-- begin row -->
        <div class="row">
            <div class="col-md-12 ui-sortable">
                <div class="panel panel-inverse" data-sortable-id="ui-buttons-1">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                        </div>
                        <h4 class="panel-title">Foydalanuvchi</h4>
                    </div>
                    <div class="panel-body">
                        <div class="note note-success">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <tbody>
                                            <tr>
                                                <th width="250px">FIO</th>
                                                <td colspan="2">{{$user->fio??'_'}}</td>
                                                <th>Tug'ilgan kun</th>
                                                <td colspan="2">{{ $user->birthday ?? '_' }}</td>
                                            </tr>
                                            <tr>
                                                <th>Login</th>
                                                <td colspan="2">{{$user->username??'_'}}</td>
                                                <th>Telefon</th>
                                                <td colspan="2">{{$user->phone??'_'}}</td>
                                            </tr>
                                            <tr>
                                                <th>Jinsi</th>
                                                <td colspan="2">@if($user->sex == 1)Erkak @else @if($user->sex == 0)Ayol @else _ @endif @endif</td>
                                                <th>Status</th>
                                                <td colspan="2">@if($user->status == 1)Faol @else Faol emas @endif</td>
                                            </tr>
                                            <tr>
                                                <th>Fakultet</th>
                                                <td colspan="2">{{$user->faculty->name??'_'}}</td>
                                                <th>Rol</th>
                                                <td colspan="2">{{$user->role->name??'_'}}</td>
                                            </tr>
                                            <tr>
                                                <th>Yillik re'ja</th>
                                                <td colspan="2">
                                                    @if($user->annual_plan)
                                                        <a href="{{route('users.download',['documents', $user->annual_plan])}}" title="Yillik re'ja" class="btn btn-xs btn-primary">
                                                            <i class="fa fa-download"></i> {{$user->annual_plan}}
                                                        </a>
                                                    @else _
                                                    @endif
                                                </td>
                                                <th>Obyektivka (CV)</th>
                                                <td colspan="2">
                                                    @if($user->cv)
                                                        <a href="{{route('users.download',['documents', $user->cv])}}" title="Obyektivka" class="btn btn-xs btn-primary">
                                                            <i class="fa fa-download"></i> {{$user->cv}}
                                                        </a>
                                                    @else _
                                                    @endif
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>Diplom</th>
                                                <td colspan="2">
                                                    @if($user->diplom)
                                                        <a href="{{route('users.download',['documents', $user->diplom])}}" title="Diplom" class="btn btn-xs btn-primary">
                                                            <i class="fa fa-download"></i> {{$user->diplom}}
                                                        </a>
                                                    @else _
                                                    @endif
                                                </td>
                                                <th>Passport</th>
                                                <td colspan="2">
                                                    @if($user->passport)
                                                        <a href="{{route('users.download',['documents', $user->passport])}}" title="Obyektivka" class="btn btn-xs btn-primary">
                                                            <i class="fa fa-download"></i> {{$user->passport}}
                                                        </a>
                                                    @else _
                                                    @endif
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label class="control-label">Profil rasmi</label>
                                    <img id="preview" class="img-thumbnail"
                                         @if($user->image) src="{{asset('storage/users/'.$user->image)}}"  @else src="{{asset('img/noimg.jpg')}}" @endif
                                         alt=""
                                         style="object-fit: cover; width:100%; height: 300px;">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <a class="btn btn-inverse" href="{{route('users.index')}}" data-pjax="0"
                               style="margin: 2px;"><i class="fa fa-angle-double-left"></i> Ortga</a>
                            <a class="btn btn-info" href="{{route('users.edit',['user' => $user])}}" data-pjax="0"
                               style="margin: 2px;"><i class="fa fa-pencil"></i> Tahrirlash</a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div>
@endsection

