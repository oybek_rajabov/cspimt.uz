@extends('layouts.app')
@section('content')
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-left" style="margin-bottom: 10px;">
            <li class="breadcrumb-item"><a href="/">Asosiy</a></li>
            <li class="breadcrumb-item active">Foydalanuvchilar</li>
        </ol>
        <!-- begin row -->
        @include('users.modals')
        <div class="row">
            <div class="col-md-12 ui-sortable">
                <div class="panel panel-inverse" data-sortable-id="ui-buttons-1">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a onclick="showCreateModal()"  class="btn btn-xs btn-success">
                                Qo'shish <i class="fa fa-plus"></i>
                            </a>
                        </div>
                        <h4 class="panel-title">Foydalanuvchilar</h4>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            @include('users.columns')
                        </div>
                </div>
            </div>
        </div>

        <!-- end row -->
    </div>
@endsection
@include('users.indexJs')
