<form action="{{route('users.update', ['user' => $user->id])}}" method="post" id="profileUpdateForm"
      autocomplete="off" enctype="multipart/form-data">
    @csrf
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group field-users-fio">
                            <label class="control-label" for="fio">FIO</label>
                            <input type="text" class="form-control" id="fio" name="fio"
                                   value="{{$user->fio ?? old('fio')}}">
                            @error('fio')
                            <div class="alert-danger">{{$message}}</div>
                            @enderror
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group field-users-phone">
                            <label class="control-label" for="users-phone">Telefon</label>
                            <input type="text" id="phone" class="form-control phone_number" name="phone"
                                   placeholder="+998-99-999-99-99"
                                   value="{{$user->phone ?? old('phone')}}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label">Login</label>
                            <input type="text" class="form-control" id="username" name="username"
                                   value="{{$user->username ?? old('username')}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group ">
                            <label class="control-label">Yangi parol</label>
                            <input type="text" class="form-control" id="newpassword" name="newpassword">
                        </div>
                    </div>
                </div>
                @if(auth()->user()->role->key == 'admin')
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group field-users-sex">
                                <label class="control-label" for="users-status">Status</label>
                                <select id="status" class="form-control" name="status">
                                    <option value="">Statusni tanlang..</option>
                                    <option value="1"
                                            @if($user->status == 1) selected @endif>Aktiv
                                    </option>
                                    <option value="0"
                                            @if($user->status == 0) selected @endif>Aktiv emas
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group field-users-sex">
                                <label class="control-label" for="role">Rol</label>
                                <select id="role" class="form-control" name="role">
                                    <option value="">Rol tanlang...</option>
                                    @foreach($roles as $role)
                                        <option value="{{$role->id}}"
                                                @if($user->role_id == $role->id) selected @endif>{{$role->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group field-users-sex">
                                <label class="control-label" for="faculty">Fakultet</label>
                                <select id="faculty" class="form-control" name="faculty">
                                    <option value="">Tanlang...</option>
                                    @foreach($faculties as $faculty)
                                        <option value="{{$faculty->id}}"
                                                @if($user->faculty_id == $faculty->id) selected @endif>{{$faculty->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group field-users-fio">
                            <label class="control-label" for="datepicker-default">Tug'ilgan sana</label>
                            <input type="text" class="form-control" id="datepicker-default"
                                   name="birthday"
                                   value="{{$user->birthday ? date('Y-m-d', strtotime($user->birthday)) : ''}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group field-users-sex">
                            <label class="control-label" for="users-sex">Jinsi</label>
                            <select id="sex" class="form-control" name="sex">
                                <option value="">Tanlang..</option>
                                <option value="1"
                                        @if($user->sex == 1) selected @endif>Erkak
                                </option>
                                <option value="0"
                                        @if($user->sex == 0) selected @endif>Ayol
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4" style="text-align: center;">
                <img id="preview" class="img-thumbnail"
                     @if($user->image) src="{{asset('storage/users/'.$user->image)}}"
                     @else src="{{asset('img/noimg.jpg')}}" @endif alt=""
                     style="object-fit: cover; min-width:80%; min-height:250px; max-height: 300px;">
                <div class="">
                    <label class="btn btn-info" style="min-width:80%;" for="p_image"><span
                            class="fa fa-camera"></span> Profil rasmi</label>
                    <input type="file" id="p_image" class="image_input" name="image" accept="image/*"
                           style="display: none;"
                           onchange="document.getElementById('preview').src = window.URL.createObjectURL(this.files[0])">
                    <div class="help-block"></div>
                </div>
            </div>
        </div>
</form>
<div id="errorAlert" style="display: none;">

</div>
