@extends('layouts.app')
@section('content')
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-left" style="margin-bottom: 10px;">
            <li class="breadcrumb-item"><a href="/">Asosiy</a></li>
            <li class="breadcrumb-item"><a href="{{route('users.index')}}">Foydalanuvchilar</a></li>
            <li class="breadcrumb-item active">Tahrirlash</li>
        </ol>
        <!-- begin row -->
        <div class="row">
            <div class="col-md-12 ui-sortable">
                <div class="panel panel-inverse" data-sortable-id="ui-buttons-1">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                        </div>
                        <h4 class="panel-title">Tahrirlash</h4>
                    </div>
                    <div class="panel-body">
                        <form action="{{route('users.update', ['user' => $user->id])}}" method="post"
                              autocomplete="off" enctype="multipart/form-data">
                            @csrf
                            @method('PUT')
                            <div>
                                @if ($errors->any())
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group field-users-fio">
                                                    <label class="control-label" for="fio">FIO</label>
                                                    <input type="text" class="form-control" id="fio" name="fio"
                                                           value="{{$user->fio ?? old('fio')}}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group field-users-phone">
                                                    <label class="control-label" for="users-phone">Telefon</label>
                                                    <input type="text" id="phone" class="form-control phone_number" name="phone"
                                                           placeholder="+998-99-999-99-99" value="{{$user->phone ?? old('phone')}}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label class="control-label">Login</label>
                                                    <input type="text" class="form-control" id="username" name="username"
                                                           value="{{$user->username ?? old('username')}}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group ">
                                                    <label class="control-label">Yangi parol</label>
                                                    <input type="text" class="form-control" id="newpassword" name="newpassword">
                                                </div>
                                            </div>
                                        </div>
                                        @if(auth()->user()->role->key == 'admin')
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group field-users-sex">
                                                        <label class="control-label" for="users-status">Status</label>
                                                        <select id="status" class="form-control" name="status">
                                                            <option value="">Statusni tanlang..</option>
                                                            <option value="1"
                                                                    @if($user->status == 1) selected @endif>Aktiv</option>
                                                            <option value="0"
                                                                    @if($user->status == 0) selected @endif>Aktiv emas</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group field-users-sex">
                                                        <label class="control-label" for="role">Rol</label>
                                                        <select id="role" class="form-control" name="role">
                                                            <option value="">Rol tanlang...</option>
                                                            @foreach($roles as $role)
                                                                     <option value="{{$role->id}}" @if($user->role_id == $role->id) selected @endif>{{$role->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-md-4" style="text-align: center;">
                                        <img id="preview" class="img-thumbnail" @if($user->image) src="{{asset('storage/users/'.$user->image)}}"  @else src="{{asset('img/noimg.jpg')}}" @endif alt="" style="object-fit: cover; min-width:80%; min-height:180px; max-height: 180px;">
                                        <div class="">
                                            <label class="btn btn-info" style="min-width:80%;" for="image"><span class="fa fa-camera"></span> Profil rasmi</label>
                                            <input type="file" id="image" class="image_input" name="image" accept="image/*" style="display: none;" onchange="document.getElementById('preview').src = window.URL.createObjectURL(this.files[0])">
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group pull-right">
                                        <a class="btn btn-inverse" href="{{route('users.index')}}"><span
                                                class="fa fa-angle-double-left"></span> Ortga</a>
                                        <button type="submit" class="btn btn-success">Saqlash</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- end row -->
        </div>
@endsection
        @section('customJs')
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js"></script>
            <script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
            <script>
                $(document).ready(function () {
                    $("#datepicker-default").datepicker({format:'dd.mm.yyyy'})
                    $('.phone_number').inputmask('+\\9\\9899-999-99-99');
                    $('#fio').on('input', function () {
                        $(this).next().css('display', 'none');
                    });
                    $('#phone').on('input', function () {
                        $(this).next().css('display', 'none');
                    });
                    $('#email').on('input', function () {
                        $(this).next().css('display', 'none');
                    });
                    $('#newpassword').on('input', function () {
                        $(this).next().css('display', 'none');
                    });
                    $('#status').on('change', function () {
                        $(this).next().css('display', 'none');
                    });
                    $('#role').on('change', function () {
                        $(this).next().css('display', 'none');
                    });
                });
            </script>
@endsection
