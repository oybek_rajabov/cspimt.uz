<div class="modal fade" id="delete_modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Amalni tasdiqlang</h4>
            </div>
            <div class="modal-body">
                <h4>Rostdan ham o'chirishni xohlaysizmi?</h4>
            </div>
            <form method="POST" id="delete_form" >
                @csrf
                <input id="id_delete" name="id" type="hidden">
                <div class="modal-footer">
                    <a href="javascript:;" class="btn btn-sm btn-white pull-left" data-dismiss="modal">Yo'q</a>
                    <button type="submit" class="btn btn-sm btn-danger">Ha</button>
                </div>
            </form>

        </div>
    </div>
</div>
