<form action="#" id="actionUpdateForm" class="form-horizontal">
    @csrf
    <div class="row">
        <div class="col-md-12">
            <div>
                <label for="name"> Ta'lim shakli</label>
                <select name="talimturi_id" id="talimturi_id" class="form-control">
                    <option value="" selected>Tanlang..</option>
                    @foreach(talimturlari() as $talimturi)
                        <option value="{{$talimturi->id}}" @if($talimturi->id == $yonalishtalimturi->talim_turi_id) selected @endif>{{$talimturi->name}}</option>
                    @endforeach
                </select>
                <div class="help-block"></div>
            </div>
        </div>
        <div class="col-md-12">
            <div>
                <label for="name"> Yo'nalish</label>
                <select name="speciality_id" id="speciality_id" class="form-control">
                    <option value="" selected>Tanlang..</option>
                    @foreach(specialities() as $speciality)
                        <option value="{{$speciality->id}}" @if($speciality->id == $yonalishtalimturi->yonalish_id)selected @endif>{{$speciality->name}}</option>
                    @endforeach
                </select>
                <div class="help-block"></div>
            </div>
        </div>
    </div>
    <div id="errorAlert" style="display: none;">

    </div>
</form>
