<form method="get" action="{{route('admin.yonalishtalimturi.index')}}">
    @csrf
    <th></th>
    <th>
        <select name="talimturiId" id="talimturiId" class="form-control">
            <option value="" selected>Tanlang..</option>
            @foreach(talimturlari() as $talimturi)
                <option value="{{$talimturi->id}}">{{$talimturi->name}}</option>
            @endforeach
        </select>
    </th>
    <th>
        <select name="specialityId" id="specialityId" class="form-control">
            <option value="" selected>Tanlang..</option>
            @foreach(specialities() as $speciality)
                <option value="{{$speciality->id}}">{{$speciality->name}}</option>
            @endforeach
        </select>
    </th>
    <th>
        <button type="submit" class="btn btn-warning btn-sm">
            <i class="fa fa-filter"></i> Qidirish
        </button>
    </th>
</form>
