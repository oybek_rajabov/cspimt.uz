@extends('layouts.app')

@section('content')
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-left">
            <li><a href="javascript:;">Asosiy</a></li>
            <li class="active">Statistika</li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <br>
        <br>
        <h1 class="page-header">Statistika <small>marketing faoliyati va talabalar bo'yicha statistikalar</small></h1>
        <!-- end page-header -->
        <!-- begin row -->
        <div class="row">
            <!-- begin col-3 -->
            <div class="col-md-3 col-sm-6">
                <div class="widget widget-stats bg-green">
                    <div class="stats-icon"><i class="fa fa-users"></i></div>
                    <div class="stats-info">
                        <h4>Guruhlar soni</h4>
                        <p>5</p>
                        <div class="stats-link">
                            <a href="#">Batafsil <i class="fa fa-arrow-circle-o-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end col-3 -->
            <!-- begin col-3 -->
            <div class="col-md-3 col-sm-6">
                <div class="widget widget-stats bg-blue">
                    <div class="stats-icon"><i class="fa fa-user"></i></div>
                    <div class="stats-info">
                        <h4>O'quvchilar soni</h4>
                        <p>5</p>
                        <div class="stats-link">
                            <a href="#">Batafsil <i class="fa fa-arrow-circle-o-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end col-3 -->
            <!-- begin col-3 -->
            <div class="col-md-3 col-sm-6">
                <div class="widget widget-stats bg-purple">
                    <div class="stats-icon"><i class="fa fa-circle"></i></div>
                    <div class="stats-info">
                        <h4>Foydalanuvchilar soni</h4>
                        <p>5</p>
                        <div class="stats-link">
                            <a href="#">Batafsil <i class="fa fa-arrow-circle-o-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end col-3 -->
            <!-- begin col-3 -->
            <div class="col-md-3 col-sm-6">
                <div class="widget widget-stats bg-red">
                    <div class="stats-icon"><i class="fa fa-briefcase"></i></div>
                    <div class="stats-info">
                        <h4>Vazifalar soni</h4>
                        <p>5</p>
                        <div class="stats-link">
                            <a href="#">Batafsil <i class="fa fa-arrow-circle-o-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end col-3 -->
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse" data-sortable-id="ui-general-3">
                    <div class="panel-heading">
                        <h4 class="panel-title">Tyutorlar harakati</h4>
                    </div>
                    <div class="panel-body">
                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th>FIO</th>
                                <th>Ish jarayonida</th>
                                <th>Tekshirilmoqda</th>
                                <th>Rad etilgan</th>
                                <th>Qabul qilingan</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($datas as $value)
                                <tr>
                                    <td>{{$value->fio}}</td>
                                    <td>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-primary" style="width: 5%">5</div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-warning" style="width: 5%">5</div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-danger" style="width: 5%">5</div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-success" style="width: 5%">5</div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{$datas->links('pagination::bootstrap-4')}}
                    </div>
                </div>
            </div>
        </div>
        <!-- end row -->
@endsection
