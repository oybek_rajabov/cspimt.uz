<form action="#" id="actionUpdateForm" class="form-horizontal">
    @csrf
    <div class="row">
        <div class="col-md-12">
            <div>
                <label for="name"> Nomi</label>
                <input type="text" id="name"  name="name" value="{{$talimturi->name ?? ''}}" class="form-control">
                <div class="help-block"></div>
            </div>
        </div>
    </div>
    <div id="errorAlert" style="display: none;">

    </div>
</form>
