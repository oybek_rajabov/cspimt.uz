<div class="modal fade in" id="fileUploadModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="fileUploadLabel">Talaba import</h5>
            </div>
            <div class="modal-body">
                <form action="#" id="importForm" class="form-horizontal" enctype="multipart/form-data" >
                    @csrf
                    <div class="form-group">
                        <label class="col-md-3 control-label">Talabalar fayli</label>
                        <div class="col-md-7">
                            <input type="file" name="students_file" accept=".xls,.xlsx" class="form-control input-sm" placeholder="Faylni tanlang"/>
                        </div>
                    </div>
                    <div id="errorAlert" style="display: none;">

                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-white"
                        data-dismiss="modal">Yopish</button>
                <a href="#" id="importSubmit" class="btn btn-sm btn-success">Yuklash</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="deleteModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="shartnomagaForm">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>
