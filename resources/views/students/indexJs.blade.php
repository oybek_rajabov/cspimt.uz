
@section('customJs')
    <script>
        function showFileUpload()
        {
            var modal = $('#fileUploadModal');
            modal.modal('show');
        }
        $(document).on('click', '#importSubmit',function (e) {
            e.preventDefault();
            var form = new FormData($('#importForm')[0]);
            $.ajax({
                method: 'POST',
                url: '{{route('students.import-students')}}',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: form,
                processData: false,
                contentType: false,
                success: function (response) {
                    $('#fileUploadModal').modal('hide');
                    $.gritter.add({
                        title: "Muvaffaqiyatli yakunladi",
                        text: response.success,
                        class_name: "gritter-light",
                        image: '{{asset('assets/img/success.png')}}',
                        sticky: false,
                        time: 200,
                        // after_close: function () {
                        //     window.location.reload();
                        // }
                    });
                    window.location.reload();

                },
                error: function (response){
                    var message = JSON.parse(response.responseText).message['error'][0];
                    $('#fileUploadModal').modal('hide');
                    $.gritter.add({
                        title: "Xatolik",
                        text: message,
                        class_name: "gritter-light",
                        image: '{{asset('assets/img/error.png')}}',
                        sticky: false,
                        time: 5000,
                    });

                }
            });
        })


        function showDeleteModal(id) {
            var modal = $('#deleteModal');
            $('#createModal').modal('hide');
            $('#updateModal').modal('hide');
            $.ajax({
                method: 'GET',
                url:'/marketing_xodim/students/delete/'+id,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function (response){
                    modal.find('.modal-header').html(response.header);
                    modal.find('.modal-body').html(response.content);
                    modal.find('.modal-footer').html(response.footer);
                    modal.modal('show');
                },
                error: function (response) {
                    $.gritter.add({
                        title: "Xatolik",
                        text: response.message,
                        class_name: "gritter-light",
                        image: '{{asset('assets/img/error.png')}}',
                        sticky: false,
                        time: 3000
                    });
                }
            });
        }

        function deleteStudent(id){
            $.ajax({
                method: 'delete',
                url: '/marketing_xodim/students/delete/'+id,
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success: function (response) {
                    $.gritter.add({
                        title: "Muvaffaqiyatli yakunladi",
                        text: response.success,
                        class_name: "gritter-light",
                        image: '{{asset('assets/img/success.png')}}',
                        sticky: false,
                        time: 200,
                    });
                    window.location.reload();

                },
                error: function (response){
                    $.gritter.add({
                        title: "Xatolik",
                        text: response.message,
                        class_name: "gritter-light",
                        image: '{{asset('assets/img/error.png')}}',
                        sticky: false,
                        time: 200
                    });

                }
            });

        }
        $(document).on('click','#deleteAllButton',function () {
            if ($(this).is(':checked', true)) {
                $(".sub_chk").prop('checked', true);
            } else {
                $(".sub_chk").prop('checked', false);
            }
        })

        $(document).on('click','.delete_all',function () {
                var allVals = [];
                $(".sub_chk:checked").each(function () {
                    allVals.push($(this).attr('data-id'));
                });

                if (allVals.length <= 0) {
                    alert("Hech narsa tanlanmadi!");
                } else {
                    if(confirm('Tanlanganlarni o\'chirilsinmi?') == true){
                        var join_selected_values = allVals.join(",");
                        $.ajax({
                            url: '{{route('students.delete-all')}}',
                            data: {
                                'ids': join_selected_values
                            },
                            type: 'POST',
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            dataType: 'JSON',
                            success: function (data) {
                                if (data['success']) {
                                    $(".sub_chk:checked").each(function () {
                                        $(this).parents("tr").remove();
                                    });
                                    alert(data['success'])
                                    setTimeout(function () {
                                        window.location.reload(1);
                                    }, 1000);
                                } else if (data['error']) {
                                    alert(data['error']);
                                } else {
                                    alert('{{__('message.whoops')}}');
                                }
                            },
                            error: function (data) {
                                alert(data.responseText);
                            }
                        });


                        $.each(allVals, function (index, value) {
                            $('table tr').filter("[tr_='" + value + "']").remove();
                        });
                    }

                }
        })


        function showShartnomaTuri()
        {
            var allVals = [];
            $(".sub_chk:checked").each(function () {
                allVals.push($(this).attr('data-id'));
            });
            if (allVals.length <= 0) {
                alert("Hech narsa tanlanmadi!");
            }else{
                var join_selected_values = allVals.join(",");
                var modal1 = $('#shartnomagaForm');
                $.ajax({
                    method: 'POST',
                    url:'{{route('students.shartnoma-turi')}}',
                    data: {
                        'ids': join_selected_values
                    },
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    success: function (response){
                        modal1.find('.modal-header').html(response.header);
                        modal1.find('.modal-body').html(response.content+response.footer);
                        // modal1.find('.modal-footer').html();
                        modal1.modal('show');
                        func();
                    },
                    error: function (response) {
                        $.gritter.add({
                            title: "Xatolik",
                            text: response.message,
                            class_name: "gritter-light",
                            image: '{{asset('assets/img/error.png')}}',
                            sticky: false,
                            time: 3000
                        });
                    }
                });
            }

        }
        $(document).on('click', '#shartnomaSubmit',function (e) {
            e.preventDefault();
            var formData = $('#actionShartnomaga').serialize();
            // console.log(formData)
            {{--$.ajax({--}}
            {{--    method: 'POST',--}}
            {{--    url: '{{route('students.shartnomaga')}}',--}}
            {{--    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},--}}
            {{--    data: formData,--}}
            {{--    success: function (response) {--}}
            {{--        $('#createModal').modal('hide');--}}
            {{--        $.gritter.add({--}}
            {{--            title: "Muvaffaqiyatli yakunladi",--}}
            {{--            text: response.success,--}}
            {{--            class_name: "gritter-light",--}}
            {{--            image: '{{asset('assets/img/success.png')}}',--}}
            {{--            sticky: false,--}}
            {{--            time: 200,--}}
            {{--            // after_close: function () {--}}
            {{--            //     window.location.reload();--}}
            {{--            // }--}}
            {{--        });--}}
            {{--        window.location.reload();--}}

            {{--    },--}}
            {{--    error: function (response){--}}
            {{--        var errors = JSON.parse(response.responseText).errors;--}}
            {{--        var errorsHtml = '<ul>';--}}
            {{--        $.each(errors, function (index, value) {--}}
            {{--            errorsHtml += '<li><p class="text-danger">' + value + '</p></li>';--}}
            {{--        });--}}
            {{--        errorsHtml +='</ul>';--}}
            {{--        $('#errorAlert').css('display','block');--}}
            {{--        $('#errorAlert').html(errorsHtml);--}}

            {{--    }--}}
            {{--});--}}
        })
    </script>
@endsection
