@extends('layouts.app')
@section('content')
	<div id="content" class="content">
		<!-- begin breadcrumb -->
		<ol class="breadcrumb pull-left" style="margin-bottom: 10px;">
			<li class="breadcrumb-item"><a href="{{route('students.index')}}">Talabalar</a></li>
			<li class="breadcrumb-item active">Yangi talaba qo'shish</li>
		</ol>
		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<form action="{{route('students.store')}}" method="POST" class="form-horizontal"
				enctype="multipart/form-data">
			@csrf
			<div class="row">
				<div class="col-md-12 ui-sortable">
					<div class="panel panel-inverse">
						<div class="panel-heading">
							<h4 class="panel-title">Talaba qo'shish</h4>
						</div>
						<div class="panel-body">
								<div class="row">
									<div class="col-md-8">
										<div class="row">
											<div class="col-md-6">
												<div class=" field-users-fio">
													<label class="control-label"
															 for="users-fullname">FIO</label>
													<input type="text" class="form-control" name="fio"
															 value="{{old('fio')}}">
													@error('fio')
													<div class="alert-danger">{{$message}}</div>
													@enderror
												</div>
											</div>
											<div class="col-md-6">
												<div class=" field-users-birthday">
													<label class="control-label">Millati</label>
													<input type="text" class="form-control" id="nationality"
															 name="nationality"
															 value="{{old('nationality')}}">
													@error('nationality')
													<div class="alert-danger">{{$message}}</div>
													@enderror
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<div class=" field-users-phone">
													<label class="control-label"
															 for="users-phone">Telefon raqami</label>
													<input type="text" id="telefon" class="form-control phone_number"
															 name="telefon"
															 value="{{old('telefon')}}">
													@error('telefon')
													<div class="alert-danger">{{$message}}</div>
													@enderror
												</div>
											</div>
											<div class="col-md-6">
												<div class="">
													<label class="control-label">Email</label>
													<input type="text" class="form-control" id="email" name="email"
															 value="{{old('email')}}">
													@error('email')
													<div class="alert-danger">{{$message}}</div>
													@enderror
												</div>
											</div>

										</div>
										<div class="row">
											<div class="col-md-6">
												<label class="control-label">Passport seriasi</label>
												<input type="text" class="form-control" id="series" name="p_seria"
														 value="{{old('p_seria')}}">
												@error('p_seria')
												<div class="alert-danger">{{$message}}</div>
												@enderror
											</div>
											<div class="col-md-6">
												<label
														  class="control-label">Passport raqami</label>
												<input type="text" class="form-control" id="number" name="p_number"
														 value="{{old('p_number')}}">
												@error('p_number')
												<div class="alert-danger">{{$message}}</div>
												@enderror
											</div>
										</div>
										<div class="row">
											<div class="col-md-6">
												<label class="control-label">Jinsi</label>
												<select name="jins" class="form-control">
													<option value="">Jinsini tanlang</option>
													<option value="true" @if(old('jins')) selected @endif>Erkak</option>
													<option value="false" @if(old('jins') === false) selected @endif>Ayol</option>
												</select>
											</div>
											<div class="col-md-6">
												<label class="control-label">Kursi</label>
												<input type="number" min="1" class="form-control" id="series"
														 name="course"
														 value="{{old('course')}}">
												@error('course')
												<div class="alert-danger">{{$message}}</div>
												@enderror
											</div>
										</div>
										<div class="row">
											<div class="col-md-12">
												<label class="control-label">Yashash joyi</label>
												<textarea class="form-control" name="address" cols="30"
															 rows="2">{{old('address')}}</textarea>
											</div>
										</div>

									</div>
									<div class="col-md-4">
										<label class="control-label">Fakulteti</label>
										<select name="faculty_id" class="form-control" onchange="selectSpecialty(this)">
											<option value="0">Tanlang</option>
											@foreach($faculties as $value)
												<option value="{{$value->id}}"
														  @if(old('faculty_id') == $value->id) selected @endif>{{$value->name}}</option>
											@endforeach
										</select>
										@error('faculty_id')
										<div class="alert-danger">{{$message}}</div>
										@enderror
									</div>
									<div class="col-md-4">
										<label class="control-label">Yo'nalishni</label>
										<select name="speciality_id" class="form-control" id="selectYonalish">
											@if(old('faculty_id'))
												@php $specialities = \App\Models\Student::getSpeciality(old('faculty_id')) @endphp
												@foreach($specialities as $value)
													<option value="{{$value->id}}"
															  @if(old('speciality_id') == $value->id) selected @endif>{{$value->name}}</option>
												@endforeach
											@else
												<option value="">Tanlang</option>
											@endif
										</select>
										@error('speciality_id')
										<div class="alert-danger">{{$message}}</div>
										@enderror
									</div>
									<div class="col-md-4">
										<label class="control-label">Talim shakli</label>
										<select name="talim_turi_id" class="form-control">
											<option value="0">Tanlang</option>
											@foreach($talimturlari as $value)
												<option value="{{$value->id}}"
														  @if(old('talim_turi_id') == $value->id) selected @endif>{{$value->name}}</option>
											@endforeach
										</select>
										@error('talim_turi_id')
										<div class="alert-danger">{{$message}}</div>
										@enderror
									</div>
									<div class="col-md-4">
										<label class="control-label">Talim tili</label>
										<select name="talim_tili_id" class="form-control">
											<option value="0">Tanlang</option>
											@foreach($talimtillari as $value)
												<option value="{{$value->id}}"
														  @if(old('talim_tili_id') == $value->id) selected @endif>{{$value->name}}</option>
											@endforeach
										</select>
										@error('talim_tili_id')
										<div class="alert-danger">{{$message}}</div>
										@enderror
									</div>
									<div class="col-md-4">
										<label class="control-label">Kontrakt turi</label>
										<select name="kontraktturi_id" class="form-control">
											<option value="0">Tanlang</option>
											@foreach($kontraktturlari as $value)
												<option value="{{$value->id}}"
														  @if(old('kontraktturi_id') == $value->id) selected @endif>{{$value->name}}</option>
											@endforeach
										</select>
										@error('kontraktturi_id')
										<div class="alert-danger">{{$message}}</div>
										@enderror
									</div>
								</div>
							<div class="row m-t-10">
								<div class="col-md-12">
									<a href="{{route('students.index')}}" class="btn btn-default pull-left">Bekor
										qilish
									</a>
									<button type="submit" class="btn btn-primary pull-left m-l-20">Saqlash</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
@endsection

@section('customJs')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js"></script>

	<script>
       function selectSpecialty(selectObject) {
           var id = selectObject.value;
           if (id) {
               $.ajax({
                   type: "get",
                   url: "{{route('students.specialty')}}",
                   data: {'id': id},
                   cache: false,
                   success: function (data) {
                       select = document.getElementById('selectYonalish');
                       select.innerHTML = data;
                   },
                   error: function (data) {
                       select = document.getElementById('selectYonalish');
                       select.innerHTML = " <option value=''>Tanlang</option>";
                   }
               });
           }
       }


       $(document).ready(function () {
           $('#series').inputmask({"mask": "AA"});
           $('#number').inputmask({"mask": "9999999"});
           $('.phone_number').inputmask('+\\9\\9899-999-99-99');
           $('.phone_father').inputmask('+\\9\\9899-999-99-99');
           $('.phone_mather').inputmask('+\\9\\9899-999-99-99');
           $('#fullname').on('input', function () {
               $(this).next().css('display', 'none');
           });
           $('#phone').on('input', function () {
               $(this).next().css('display', 'none');
           });
           $('#email').on('input', function () {
               $(this).next().css('display', 'none');
           });
           $('#password').on('input', function () {
               $(this).next().css('display', 'none');
           });
           $('#birthday').on('change', function () {
               $(this).next().css('display', 'none');
           });
           $('#status').on('change', function () {
               $(this).next().css('display', 'none');
           });
           $('#permission').on('change', function () {
               $(this).next().css('display', 'none');
           });
       });
	</script>
@endsection


