@extends('layouts.app')
@section('galleryCss')
    <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
    <link href="{{asset('assets/plugins/isotope/isotope.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/plugins/lightbox/css/lightbox.css')}}" rel="stylesheet" />
    <!-- ================== END PAGE LEVEL STYLE ================== -->

    <!-- ================== BEGIN BASE JS ================== -->
    <script src="{{asset('assets/plugins/pace/pace.min.js')}}"></script>
    <!-- ================== END BASE JS ================== -->
@endsection
@section('content')
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-left" style="margin-bottom: 10px;">
            <li class="breadcrumb-item"><a href="{{route('tasks.index')}}">Topshiriqlar</a></li>
            <li class="breadcrumb-item active">Topshiriq</li>
        </ol>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-inverse" data-sortable-id="form-plugins-9">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                        </div>
                        <h4 class="panel-title">Topshiriq</h4>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                @foreach($languages as $language)
                                    <tr>
                                        <th style="width: 15%;">{{__('message.description_'.$language->url)}}</th>
                                        <th><span style="font-weight: 400;  white-space: pre-wrap;">{{$task->getTranslation('description',$language->url)}}</span></th>
                                    </tr>
                                @endforeach
                                <tr>
                                    <th style="width: 15%;">Boshlangan vaqt</th>
                                    <th><span style="font-weight: 400; white-space: pre-wrap;">{{$task->beginning}}</span></th>
                                </tr>
                                <tr>
                                    <th style="width: 15%;">Tugash vaqti</th>
                                    <th><span style="font-weight: 400; white-space: pre-wrap;">{{$task->deadline}}</span></th>
                                </tr>
                                <tr>
                                    <th style="width: 15%;">Fayllar</th>
                                    <th>
                                        @foreach($documents as $document)
                                            <a href="{{route('tasks.download',['folder' => 'document_files','file' => $document])}}" title="topshiriq fayli" class="btn btn-xs btn-primary">
                                                <i class="fa fa-download"></i> {{$document}}
                                            </a>
                                        @endforeach
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>

                        <div id="gallery" class="gallery isotope" style="position: relative; overflow: hidden; height: 1047px;">
                            @foreach($images as $image)
                                <div class="image gallery-group-1 isotope-item" style="position: absolute; left: 0px; top: 0px; transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1); opacity: 1;">
                                    <div class="image-inner">
                                        <a href="{{asset('storage/image_files/'.$image)}}" data-lightbox="gallery-group-1">
                                            <img src="{{asset('storage/image_files/'.$image)}}" alt="">
                                        </a>
                                        <p class="image-caption">
                                            {{$image}}
                                        </p>
                                    </div>
                                    <div class="image-info">
                                        <a class="btn btn-success" href="{{route('tasks.download',['folder' => 'image_files','file' => $image])}}"><i class="fa fa-download"></i>Yuklash</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <div class="form-group pull-right">
                            <a class="btn btn-inverse" href="{{route('tasks.index')}}" data-pjax="0" style="margin: 2px;"><i class="fa fa-angle-double-left"></i> Ortga</a>
                            <a class="btn btn-info" href="{{route('tasks.edit', $task)}}" data-pjax="0" style="margin: 2px;"><i class="fa fa-pencil"></i> Tahrirlash</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

@endsection
@section('galleryJs')
    <script src="{{asset('assets/plugins/isotope/jquery.isotope.min.js')}}"></script>
    <script src="{{asset('assets/plugins/lightbox/js/lightbox.min.js')}}"></script>
    <script src="{{asset('assets/js/gallery.demo.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            Gallery.init();
        });
    </script>
@endsection

