@extends('layouts.app')
@section('content')
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-left" style="margin-bottom: 10px;">
            <li class="breadcrumb-item"><a href="/">Asosiy</a></li>
            <li class="breadcrumb-item active">Talabalar</li>
        </ol>

        @include('students.modals')
        <div class="row">
            <div class="col-md-12 ui-sortable">
                <div class="panel panel-inverse" data-sortable-id="ui-buttons-1">
                    <div class="panel-heading">
                        <div class="panel-heading-btn">
                            <a href="{{route('students.create')}}" class="btn btn-xs btn-success">Qo'shish <i
                                        class="fa fa-plus"></i></a>
                            <a href="#" onclick="showFileUpload()" class="btn btn-xs btn-warning"><i
                                        class="fa fa-file-excel-o"></i>&nbsp;Fayldan yuklash <i
                                        class="fa fa-plus"></i></a>
                            <a href="{{route('students.download-excel-template')}}" title="Shablon fayli" class="btn btn-xs btn-info"><i
                                        class="fa fa-file-excel-o"></i>&nbsp;Shablon <i class="fa fa-download"></i>
                            </a>
                        </div>
                        <h4 class="panel-title">Talabalar</h4>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <div class="table-responsive kv-grid-container">
                                @include('students._columns')
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!-- end row -->
        </div>
@endsection

@include('students.indexJs')

