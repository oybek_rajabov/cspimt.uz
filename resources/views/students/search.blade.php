<form method="get" action="{{route('students.index')}}" class="m-b-10">
    @csrf
<div class="row">
    <div class="col-md-10">
        <div class="row">
            <div class="col-md-4">
                <label class="control-label">FIO</label>
                <input type="text" name="fio" value="{{ request('fio') }}" class="form-control">
            </div>
            <div class="col-md-4">
                <label class="control-label">Talim turi</label>
                <select name="talimturi" class="form-control" id="">
                    <option value="">Tanlang..</option>
                    @foreach($talimturlari as $talimturi)
                        <option value="{{$talimturi->id}}" @if(request('talimturi') == $talimturi->id)selected @endif>{{$talimturi->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-4">
                <label class="control-label">Fakultet</label>
                <select name="faculty" class="form-control" id="">
                    <option value="">Tanlang..</option>
                    @foreach($faculties as $faculty)
                        <option value="{{$faculty->id}}" @if(request('faculty') == $faculty->id)selected @endif>{{$faculty->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <label class="control-label">Yo'nalish</label>
                <select name="speciality" class="form-control" id="">
                    <option value="">Tanlang..</option>
                    @foreach($specialities as $speciality)
                        <option value="{{$speciality->id}}" @if(request('speciality') == $speciality->id)selected @endif>{{$speciality->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-4">
                <label class="control-label">Kurs</label>
                <input type="text" name="course" value="{{ request('course') }}" class="form-control">
            </div>
            <div class="col-md-4 " style="margin-top: 20px;">
                <button type="submit" class="btn btn-warning btn-block">
                    <i class="fa fa-search"></i> Qidiruv
                </button>
            </div>
        </div>
    </div>
</div>
</form>
