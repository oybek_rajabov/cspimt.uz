@include('students.search')
<hr>
<div>
    <button  class="btn btn-danger btn-sm delete_all m-b-4 m-r-5">
        <i class="fa fa-trash"></i> Delete All
    </button>
    <button onclick="showShartnomaTuri()" class="btn btn-primary btn-sm m-b-4 m-r-5">
        <i class="fa fa-download"></i> Shartnomaga
    </button>
</div>

<table class="table table-bordered kv-grid-table table-striped table-condensed">
    <thead>
    <tr>
        <th style="width: 20px;"><input type="checkbox" id="deleteAllButton"></th>
        <th style="width: 20px;">@sortablelink('id','#')</th>
        <th>@sortablelink('fio','FIO')</th>
        <th>@sortablelink('talimturi','Ta\'lim shakli')</th>
        <th>@sortablelink('faculty','Fakultet')</th>
        <th>@sortablelink('speciality.name','Yo\'nalish')</th>
        <th>@sortablelink('course','Kurs')</th>
        <th style="text-align: center">Harakatlar</th>
    </tr>
    </thead>
    <tbody>
    @foreach($students  as $student)
        <tr >
            <td><input type="checkbox" class="sub_chk" data-id="{{$student->id}}"></td>
            <td>{{ $students->perPage()*($students->currentPage() - 1) + $loop->iteration }}</td>
            <td> <strong>{{$student->fio ?? ''}}</strong></td>
            <td>{{$student->talimturi->name ?? ''}}</td>
            <td>{{$student->faculty->name ?? ''}}</td>
            <td>{{$student->speciality->name ?? ''}}</td>
            <td>{{$student->kursi ?? ''}}</td>
            <td>
                <div style="display: flex; align-items: center; justify-content: center">
                    {{actionEdit(route('students.edit',$student))}}
                    {{actionModalDelete($student->id)}}
                </div>
            </td>

        </tr>
    @endforeach
    </tbody>
</table>
{{$students->links('pagination::bootstrap-4')}}
