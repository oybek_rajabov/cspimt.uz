<div class="row col-12">
    <table class="table table-striped table-bordered table-hover ">
        <thead>
        <th><label for="">Url</label></th>
        <th><label for="">Nomi</label></th>
        <th><label for="">Rol</label></th>
        <th><label for="">Status</label></th>
        <th><label for="">Rasm</label></th>
        </thead>
        <tbody>
        <tr>
            <td>{{$language->url}}</td>
            <td>{{$language->name}}</td>
            <td>@if($language->default == 1) Asosiy @else Ikkilamchi @endif</td>
            <td>@if($language->status == 1) Faol @else Faol emas @endif</td>
            <td rowspan="2">
                <img @if($language->image) src="{{asset('storage/languages/'.$language->image)}}"
                     @else src="{{asset('img/noimg.jpg')}}" @endif style="width: 100px; height: 80px" alt=""/>
            </td>
        </tr>
        </tbody>

    </table>
</div>
