<form action="#" id="actionUpdateForm" class="form-horizontal" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-md-4">
            <div>
                <img id="preview" class="img-thumbnail" @if($language->image) src="{{asset('storage/languages/'.$language->image)}}"  @else src="{{asset('img/noimg.jpg')}}" @endif alt="" style="object-fit: cover; width: 100%; height: 100%;">
                <label class="btn btn-info" style="margin-top: 10px; width: 100%;"  for="image"><span class="fa fa-camera"></span> Rasm</label>
                <input type="file" id="image"  name="image" accept="image/*" style="display: none;" onchange="document.getElementById('preview').src = window.URL.createObjectURL(this.files[0])">
                <div class="help-block"></div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="form-group">
                <label class="col-md-2 control-label">Nomi</label>
                <div class="col-md-10">
                    <input type="text" class="form-control input-sm" value="{{$language->name??''}}" placeholder="Nomi" name="name"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Url</label>
                <div class="col-md-10">
                    <input type="text" class="form-control input-sm" value="{{$language->url??''}}" placeholder="Qisqartmasi" name="url"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-2 control-label">Status</label>
                <div class="col-md-10">
                    <select name="status" class="form-control" id="">
                        <option value="">Tanlang..</option>
                        <option value="1" @if($language->status == 1) selected @endif>Faol</option>
                        <option value="0" @if($language->status == 0) selected @endif>Faol emas</option>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <div id="errorAlert" style="display: none;">

    </div>
</form>
