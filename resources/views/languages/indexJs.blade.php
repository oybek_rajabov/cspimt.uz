@section('customJs')
            <script>
                function showAction(id)
                {
                    var modal = $('#createModal');
                    $.ajax({
                        method: 'GET',
                        url:'languages/'+id,
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        success: function (response){
                            modal.find('.modal-header').html(response.header);
                            modal.find('.modal-body').html(response.content);
                            modal.find('.modal-footer').html(response.footer);
                            modal.modal('show');
                        },
                        error: function (response) {
                            $.gritter.add({
                                title: "Xatolik",
                                text: response.message,
                                class_name: "gritter-light",
                                image: '{{asset('assets/img/error.png')}}',
                                sticky: false,
                                time: 3000
                            });
                        }
                    });
                }
                function showCreateModal()
                {
                    var modal1 = $('#createModal');
                    $.ajax({
                        method: 'GET',
                        url:'{{route('languages.create')}}',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        success: function (response){
                            modal1.find('.modal-header').html(response.header);
                            modal1.find('.modal-body').html(response.content);
                            modal1.find('.modal-footer').html(response.footer);
                            modal1.modal('show');
                        },
                        error: function (response) {
                            $.gritter.add({
                                title: "Xatolik",
                                text: response.message,
                                class_name: "gritter-light",
                                image: '{{asset('assets/img/error.png')}}',
                                sticky: false,
                                time: 3000
                            });
                        }
                    });
                }
                $(document).on('click', '#createSubmit',function (e) {
                        e.preventDefault();
                        var image = document.getElementById("image").files[0];
                        var form = $('#actionUpdateForm')[0];
                        inputs = new FormData(form);
                        if(image != undefined)
                        inputs.append('image', image);
                        $.ajax({
                            method: 'POST',
                            url: '{{route('languages.store')}}',
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                            data: inputs,
                            success: function (response) {
                                $('#createModal').modal('hide');
                                $.gritter.add({
                                    title: "Muvaffaqiyatli yakunladi",
                                    text: response.success,
                                    class_name: "gritter-light",
                                    image: '{{asset('assets/img/success.png')}}',
                                    sticky: false,
                                    time: 200,
                                    // after_close: function () {
                                    //     window.location.reload();
                                    // }
                                });
                                window.location.reload();

                            },
                            error: function (response){
                                var errors = JSON.parse(response.responseText).errors;
                                var errorsHtml = '<ul>';
                                $.each(errors, function (index, value) {
                                    errorsHtml += '<li><p class="text-danger">' + value + '</p></li>';
                                });
                                errorsHtml +='</ul>';
                                $('#errorAlert').css('display','block');
                                $('#errorAlert').html(errorsHtml);

                            }
                        });
                })

                function showUpdateModal(id) {
                    var modal = $('#updateModal');
                    $('#createModal').modal('hide');
                    $.ajax({
                        method: 'GET',
                        url:'/admin/languages/'+id+'/edit',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        success: function (response){
                            modal.find('.modal-header').html(response.header);
                            modal.find('.modal-body').html(response.content);
                            modal.find('.modal-footer').html(response.footer);
                            modal.modal('show');
                        },
                        error: function (response) {
                            $.gritter.add({
                                title: "Xatolik",
                                text: response.message,
                                class_name: "gritter-light",
                                image: '{{asset('assets/img/error.png')}}',
                                sticky: false,
                                time: 3000
                            });
                        }
                    });
                }

                function updateAction(id)
                {
                    var image = document.getElementById("image").files[0];
                    var form = $('#actionUpdateForm')[0];
                    inputs = new FormData(form);
                    if(image != undefined)
                    inputs.append('image', image);
                    $.ajax({
                        method: 'POST',
                        url: '/admin/languages/update/'+id,
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: inputs,
                        processData: false,
                        contentType: false,
                        cache: false,
                        success: function (response) {
                            $('#createModal').modal('hide');
                            $.gritter.add({
                                title: "Muvaffaqiyatli yakunladi",
                                text: response.success,
                                class_name: "gritter-light",
                                image: '{{asset('assets/img/success.png')}}',
                                sticky: false,
                                time: 200,
                                // after_close: function () {
                                //     window.location.reload();
                                // }
                            });
                            window.location.reload();

                        },
                        error: function (response){
                            var errors = JSON.parse(response.responseText).errors;
                            var errorsHtml = '<ul>';
                            $.each(errors, function (index, value) {
                                errorsHtml += '<li><p class="text-danger">' + value + '</p></li>';
                            });
                            errorsHtml +='</ul>';
                            var errorAlert =  $('#errorAlert');
                            errorAlert.css('display','block');
                            errorAlert.html(errorsHtml);

                        }
                    });
                }

                function showDeleteModal(id) {
                    var modal = $('#deleteModal');
                    $('#createModal').modal('hide');
                    $('#updateModal').modal('hide');
                    $.ajax({
                        method: 'GET',
                        url:'languages/delete/'+id,
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        success: function (response){
                            modal.find('.modal-header').html(response.header);
                            modal.find('.modal-body').html(response.content);
                            modal.find('.modal-footer').html(response.footer);
                            modal.modal('show');
                        },
                        error: function (response) {
                            $.gritter.add({
                                title: "Xatolik",
                                text: response.message,
                                class_name: "gritter-light",
                                image: '{{asset('assets/img/error.png')}}',
                                sticky: false,
                                time: 3000
                            });
                        }
                    });
                }

                function deleteLanguage(id){
                    $.ajax({
                        method: 'delete',
                        url: 'languages/delete/'+id,
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        success: function (response) {
                            $.gritter.add({
                                title: "Muvaffaqiyatli yakunladi",
                                text: response.success,
                                class_name: "gritter-light",
                                image: '{{asset('assets/img/success.png')}}',
                                sticky: false,
                                time: 200,
                            });
                            window.location.reload();

                        },
                        error: function (response){
                            $.gritter.add({
                                title: "Xatolik",
                                text: response.message,
                                class_name: "gritter-light",
                                image: '{{asset('assets/img/error.png')}}',
                                sticky: false,
                                time: 200
                            });

                        }
                    });

                }
            </script>
@endsection
