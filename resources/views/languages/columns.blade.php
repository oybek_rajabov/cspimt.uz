<table class="table table-bordered">
	<thead>
	<tr>
		<th style="width: 50px;">#</th>
        <th>Url</th>
		<th>Rasm</th>
		<th>Nomi</th>
		<th>Rol</th>
		<th>Status</th>
		<th style="width:130px;">Harakatlar</th>
	</tr>
	</thead>
	<tbody>
	@foreach($langs as $lang)
		<tr>
			<td>{{ $langs->perPage()*($langs->currentPage() - 1) + $loop->iteration }}</td>
			<td>{{$lang->url}}</td>
			<td>
                <img @if($lang->image) src="{{asset('storage/languages/'.$lang->image)}}"  @else src="{{asset('img/noimg.jpg')}}" @endif style="width: 100px; height: 80px" alt="" />
            </td>
            <td>{{$lang->name}}</td>
            <td>
               @if($lang->default == 1)
                   Asosiy
                @else
                   Ikkilamchi
                @endif
            </td>
            <td>
                @if($lang->status == 1)
                    Aktiv
                @else
                    O'chirilgan
                @endif
            </td>
			<td style="display: flex;">
				{{actionModalView($lang->id)}}
				{{actionModalEdit($lang->id)}}
                @if($lang->default != 1)
				    {{actionModalDelete($lang->id)}}
                @endif
			</td>
		</tr>
	@endforeach
	</tbody>
</table>
{{$langs->links('pagination::bootstrap-4')}}
