@extends('layouts.app')
@section('dropzoneCss')
    <link href="{{asset('assets/plugins/dropzone/min/dropzone.min.css')}}" rel="stylesheet">
    <style>
        .text__block {
            position: relative;
            height: 300px;
        }

        .text__block img {
            width: 100%;
            height: 100%;
            object-fit: contain;
            object-position: center;
        }

        .text__block span.delete__file {
            position: absolute;
            top: 10px;
            right: 10px;
            border-radius: 6px;
            color: #fff;
            background-color: red;
            cursor: pointer;
            z-index: 2;
            padding: 5px 10px;
        }

        .text__block h4 {
            position: absolute;
            z-index: 2;
            bottom: 20px;
            font-weight: bold;
            left: 20px;
            right: 20px;
            text-align: center;
            background-color: rgb(238, 238, 238, 59%);
            color: #000 !important;
            padding: 10px 20px;
        }

        @media (max-width: 767px) {
            .text__block {
                height: 200px;
            }
            .text__block img{
                padding: 15px;
            }

            ul#dropzoneUploadedImages li {
                width: 50%;
            }

        }
    </style>
@endsection
@section('content')
    <div id="content" class="content">
        <!-- begin breadcrumb -->
        <ol class="breadcrumb pull-right">
            <li><a href="javascript:;">Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
        <!-- end breadcrumb -->
        <!-- begin page-header -->
        <h1 class="page-header">Dashboard <small>header small text goes here...</small></h1>
        <!-- end page-header -->
        <!-- begin row -->
        <div class="row">
            <div class="panel panel-inverse" data-sortable-id="form-dropzone-1">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                    </div>
                    <h4 class="panel-title">Dropzone</h4>
                </div>
                <div class="panel-body">
                    <div id="dropzone">
                        <form action="{{route('dropzone.store')}}" method="POST" id="dropzoneForm"
                              class="dropzone dz-clickable" id="image-upload" enctype="multipart/form-data">
                            @csrf
                            <div class="dz-message ">
                                Drop files here or click to upload.<br>
                                <span class="dz-note needsclick">
                                            (This is just a demo dropzone. Selected files are <strong>not</strong> actually uploaded.)
                                        </span>
                            </div>
                        </form>
                        <button id="submitDropzone" class="btn btn-lg btn-primary center-block"
                                style="margin-top: 10px;">
                            Upload files..
                        </button>
                    </div>
                </div>
                <div class="panel panel-inverse" data-sortable-id="index-4">
                    <div class="panel-heading">
                        <h4 class="panel-title">New Registered Users <span
                                class="pull-right label label-success">24 new users</span></h4>
                    </div>
                    <ul class="registered-users-list clearfix" id="dropzoneUploadedImages">

                    </ul>
                </div>
            </div>
        </div>
        <!-- end row -->
        @endsection
        @section('dropzoneJs')
            <script src="{{asset('assets/plugins/dropzone/min/dropzone.min.js')}}"></script>
            <script>
                Dropzone.options.dropzoneForm = {
                    autoProcessQueue: false,
                    acceptedFiles: '.png,.jpg,.docx',

                    init: function () {
                        var sumbitButton = document.querySelector("#submitDropzone");
                        myDropzone = this;

                        sumbitButton.addEventListener('click', function () {
                            myDropzone.processQueue();
                        })

                        this.on('complete', function () {
                            if (this.getQueuedFiles().length == 0 && this.getUploadingFiles().length == 0) {
                                var _this = this;
                                _this.removeAllFiles();
                            }
                            window.location.reload();
                            load_images();
                        })
                    }
                };
                load_images();

                function load_images() {
                    $.ajax({
                        method: 'GET',
                        url: '{{route('dropzone.fetch')}}',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        success: function (data) {
                            $('#dropzoneUploadedImages').html(data);
                        }
                    });
                }
            </script>
@endsection
