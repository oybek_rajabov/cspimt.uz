<li class="has-sub @if(request()->route()->getPrefix() == '/marketing_xodim')active @endif" >
    <a href="javascript:;">
        <b class="caret pull-right"></b>
        <i class="fa fa-laptop"></i>
        <span>Marketing xodimi</span>
    </a>
    <ul class="sub-menu" style="@if(request()->route()->getPrefix() == '/marketing_xodim' || request()->routeIs('marketing_xodim.students*'))display: block; @else display: none @endif">
        <li class="{{request()->routeIs('marketing_xodim.students*')?'active':''}}"><a href="{{route('marketing_xodim.students.index')}}">Talabalar</a></li>
{{--        <li class="{{request()->routeIs('groups*')?'active':''}}"><a href="{{route('groups.index')}}">Guruhlar</a></li>--}}
{{--        <li class="{{request()->routeIs('marketing_xodim.students*')?'active':''}}"><a href="{{route('marketing_xodim.students.index')}}">Talabalar</a></li>--}}
    </ul>
</li>
