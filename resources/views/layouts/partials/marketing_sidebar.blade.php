<li class="has-sub @if(request()->route()->getPrefix() == '/marketing')active @endif">
    <a href="javascript:;">
        <b class="caret pull-right"></b>
        <i class="fa fa-laptop"></i>
        <span>Marketing</span>
    </a>
    <ul class="sub-menu" style="@if(request()->route()->getPrefix() == '/prorektor')display: block; @else display: none @endif">
{{--        <li class="{{request()->routeIs('tasks.index')?'active':''}}"><a href="{{route('tasks.index')}}">Topshiriqlar</a></li>--}}
{{--        <li class="{{request()->routeIs('tasks.reports')?'active':''}}"><a href="{{route('tasks.reports')}}">Hisobotlar</a></li>--}}
    </ul>
</li>
