<div id="header" class="header navbar navbar-default navbar-fixed-top">
    <!-- begin container-fluid -->
    <div class="container-fluid">
        <!-- begin mobile sidebar expand / collapse button -->
        <div class="navbar-header">
            <a href="{{route('home')}}" class="navbar-brand" style="padding: 0 10px; display: flex; align-items: center">
                <img @if(file_exists(public_path('images/logo.png'))) src="{{asset('images/logo.png')}}" style="width: auto; max-height: 100%"  @else src="{{asset('img/noimg.jpg')}}" style="max-width: 30px; max-height: 30px; border-radius: 50%;" @endif />
                <b style="font-weight: 500;">Marketing bo'limi</b>
            </a>
        </div>
        <!-- end mobile sidebar expand / collapse button -->
        <!-- begin header navigation right -->
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown navbar-user">
                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                    <img @if(auth()->user()->image) src="{{asset('storage/users/'.auth()->user()->image)}}"  @else src="{{asset('img/noimg.jpg')}}" @endif alt="" />
                    <span class="hidden-xs">{{auth()->user()->fio}}</span> <b class="caret"></b>
                </a>
                <ul class="dropdown-menu animated fadeInLeft">
                    <li class="arrow"></li>
                    <li><a href="#" onclick="profileShow({{auth()->user()->id}})">Profil</a></li>
                    <li class="divider"></li>
                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form></li>
                </ul>
            </li>
        </ul>
        <!-- end header navigation right -->
    </div>
    <!-- end container-fluid -->
</div>
<!-- end #header -->
