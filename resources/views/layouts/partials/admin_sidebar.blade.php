<li class="has-sub @if(request()->route()->getPrefix() == '/admin' || request()->routeIs('admin*'))active @endif">
    <a href="javascript:;">
        <b class="caret pull-right"></b>
        <i class="fa fa-laptop"></i>
        <span>Admin</span>
    </a>
    <ul class="sub-menu" style="@if(request()->route()->getPrefix() == '/admin' || request()->routeIs('admin*'))display: block; @else display: none @endif">
        <li class="{{request()->routeIs('users*')?'active':''}}"><a href="{{route('users.index')}}">Foydalanuvchilar</a></li>
        <li class="{{request()->routeIs('admin.faculties*')?'active':''}}"><a href="{{route('admin.faculties.index')}}">Fakultetlar</a></li>
        <li class="{{request()->routeIs('admin.talimturi*')?'active':''}}"><a href="{{route('admin.talimturi.index')}}">Ta'lim shakllari</a></li>
        <li class="{{request()->routeIs('admin.talimtili*')?'active':''}}"><a href="{{route('admin.talimtili.index')}}">Ta'lim tillari</a></li>
        <li class="{{request()->routeIs('admin.kontraktturi*')?'active':''}}"><a href="{{route('admin.kontraktturi.index')}}">Shartnoma turlari</a></li>
        <li class="{{request()->routeIs('admin.specialities*')?'active':''}}"><a href="{{route('admin.specialities.index')}}">Yo'nalishlar</a></li>
        <li class="{{request()->routeIs('admin.yonalishtalimturi*')?'active':''}}"><a href="{{route('admin.yonalishtalimturi.index')}}">Yo'nalish vs talim turi</a></li>
    </ul>
</li>
<li class="has-sub @if(request()->route()->getPrefix() == '/marketing')active @endif">
    <a href="javascript:;">
        <b class="caret pull-right"></b>
        <i class="fa fa-laptop"></i>
        <span>Marketing</span>
    </a>
    <ul class="sub-menu" style="@if(request()->route()->getPrefix() == '/prorektor')display: block; @else display: none @endif">
{{--        <li class="{{request()->routeIs('tasks.reports') || request()->routeIs('tasks.reports.show') ? 'active':''}}"><a href="{{route('tasks.reports')}}">Hisobotlar</a></li>--}}
    </ul>
</li>
<li class="has-sub @if(request()->route()->getPrefix() == '/marketing_xodim' || request()->routeIs('marketing_xodim.students*'))active @endif" >
    <a href="javascript:;">
        <b class="caret pull-right"></b>
        <i class="fa fa-laptop"></i>
        <span>Marketing xodimi</span>
    </a>
    <ul class="sub-menu" style="@if(request()->route()->getPrefix() == '/marketing_xodim' || request()->routeIs('marketing_xodim.students*'))display: block; @else display: none @endif">
        <li class="{{request()->routeIs('marketing_xodim.students*')?'active':''}}"><a href="{{route('students.index')}}">Talabalar</a></li>
{{--        <li class="{{request()->routeIs('marketing_xodim.students*')?'active':''}}"><a href="{{route('marketing_xodim.students.index')}}">Talabalar</a></li>--}}
    </ul>
</li>
