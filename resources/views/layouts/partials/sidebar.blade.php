<!-- begin #sidebar -->
<div id="sidebar" class="sidebar">
    <!-- begin sidebar scrollbar -->
    <div data-scrollbar="true" data-height="100%">
        <!-- begin sidebar user -->
        <ul class="nav">
            <li class="nav-profile">
                <a href="javascript:" class="image"><img  style="width: 100%; height: 100%; object-fit: cover"  @if(auth()->user()->image) src="{{asset('storage/users/'.auth()->user()->image)}}"  @else src="{{asset('img/noimg.jpg')}}" @endif alt=""/></a>
                <div class="info">
                    {{auth()->user()->fio}}
                    <small>{{auth()->user()->getRoleName()}}</small>
                </div>
            </li>
        </ul>
        <!-- end sidebar user -->
        <ul class="nav">
{{--            *********************************************Admin********************************--}}
                @if(auth()->user()->role->key == 'admin')
                    @include('layouts.partials.admin_sidebar')
                @endif

{{--            ********************************Prorektor*****************************--}}
                @if(auth()->user()->role->key == 'marketing')
                       @include('layouts.partials.marketing_sidebar')
                @endif


{{--            *********************************marketing_xodim*********************************--}}
                @if(auth()->user()->role->key == 'marketing_xodim')
                       @include('layouts.partials.marketing_hodimi_sidebar')
                @endif


            <!-- begin sidebar minify button -->
            <li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
            <!-- end sidebar minify button -->
        </ul>
        <!-- end sidebar scrollbar -->

        <!-- end sidebar nav -->
    </div>

</div>
<div class="sidebar-bg"></div>
<!-- end #sidebar -->
