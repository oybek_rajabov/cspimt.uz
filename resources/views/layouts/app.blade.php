<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8"/>
    <title>CSPU marketing</title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>
    <meta content="" name="description"/>
    <meta content="" name="author"/>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <!-- ================== BEGIN BASE CSS STYLE ================== -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet"/>
    <link href="{{asset('assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/css/animate.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/css/style.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/css/style-responsive.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/css/theme/default.css')}}" rel="stylesheet" id="theme"/>
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet" id="theme"/>
    <!-- ================== END BASE CSS STYLE ================== -->
{{--    @yield('switcherCss')--}}
{{--    @yield('dropzoneCss')--}}
{{--    @yield('customCss')--}}
{{--    @yield('galleryCss')--}}
    <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
    <link href="{{asset('assets/plugins/jquery-jvectormap/jquery-jvectormap.css')}}" rel="stylesheet"/>
    <link href="{{asset('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css')}}" rel="stylesheet"/>
    @yield('daterangeCss')
    <link href="{{asset('assets/plugins/gritter/css/jquery.gritter.css')}}" rel="stylesheet"/>
    <!-- ================== END PAGE LEVEL STYLE ================== -->

    <!-- ================== BEGIN BASE JS ================== -->
    <script src="{{asset('assets/plugins/pace/pace.min.js')}}"></script>
    <style>
        .header{
            z-index: 1020;
        }
        div#page-container{
            line-height: 0;
        }
        div#page-container *{
            line-height: initial;
        }
    </style>
    <!-- ================== END BASE JS ================== -->
</head>
<body>
<div id="page-container" class="fade page-sidebar-fixed page-header-fixed in">
    @if(session()->has('error_message'))
        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-8">
                <div class="alert alert-info fade in">
                    <span class="close" data-dismiss="alert">×</span>
                    <i class="fa fa-exclamation fa-2x pull-left"></i>
                    {{session()->get('error_message')}}
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
    @else
    @endif
    @include('layouts/partials/header')
    @include('layouts/partials/sidebar');
    @yield('content')


        <div class="modal fade in" id="profileModal">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">

                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade in" id="profileUpdateModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">

                    </div>
                    <div class="modal-body">

                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>

</div>


<!-- ================== BEGIN BASE JS ================== -->
<script src="{{asset('assets/plugins/jquery/jquery-1.9.1.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery/jquery-migrate-1.1.0.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js')}}"></script>
<script src="{{asset('assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
<!--[if lt IE 9]>
<script src="{{asset('assets/crossbrowserjs/html5shiv.js')}}"></script>
<script src="{{asset('assets/crossbrowserjs/respond.min.js')}}"></script>
<script src="{{asset('assets/crossbrowserjs/excanvas.min.js')}}"></script>
<![endif]-->
<script src="{{asset('assets/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-cookie/jquery.cookie.js')}}"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="{{asset('assets/plugins/gritter/js/jquery.gritter.js')}}"></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.min.js')}}"></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.time.min.js')}}"></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.resize.min.js')}}"></script>
<script src="{{asset('assets/plugins/flot/jquery.flot.pie.min.js')}}"></script>
<script src="{{asset('assets/plugins/sparkline/jquery.sparkline.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-jvectormap/jquery-jvectormap.min.js')}}"></script>
<script src="{{asset('assets/plugins/jquery-jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
@yield('maskedInputJs')
<script src="{{asset('assets/js/dashboard.min.js')}}"></script>
@yield('dropzoneJs')
<script src="{{asset('assets/js/apps.min.js')}}"></script>
@yield('switcherJs')
@yield('galleryJs')
<!-- ================== END PAGE LEVEL JS ================== -->
@yield('customJs')

<script>
    $(window).load(function () {
        $.gritter.removeAll();
    })
    $(document).ready(function () {
        App.init();
        Dashboard.init();
    });
</script>

@yield('daterangeJs')
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-53034621-1', 'auto');
    ga('send', 'pageview');
</script>
<script>
    function profileShow(id) {
        var modal = $('#profileModal');
        $.ajax({
            method: 'GET',
            url:'/users/profile/show/'+id,
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            success: function (response){
                modal.find('.modal-header').html(response.header);
                modal.find('.modal-body').html(response.content);
                modal.find('.modal-footer').html(response.footer);
                modal.modal('show');
                func();
            },
            error: function (response) {
                $.gritter.add({
                    title: "Xatolik",
                    text: response.message,
                    class_name: "gritter-light",
                    image: '{{asset('assets/img/error.png')}}',
                    sticky: false,
                    time: 3000
                });
            }
        });
    }

    $(document).on('click', '#profileUpdateSubmit',function (e) {
        e.preventDefault();
        var p_image = document.getElementById("p_image").files[0];
        var form = $('#profileUpdateForm')[0];
        inputs = new FormData(form);
        if(p_image != undefined)
            inputs.append('image', p_image);
        $.ajax({
            method: 'POST',
            url: '/users/profile/update',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            data: inputs,
            contentType: false,
            processData: false,
            success: function (response) {
                $('#profileModal').modal('hide');
                $.gritter.add({
                    title: "Muvaffaqiyatli yakunladi",
                    text: response.success,
                    class_name: "gritter-light",
                    image: '{{asset('assets/img/success.png')}}',
                    sticky: false,
                    time: 200,
                    // after_close: function () {
                    //     window.location.reload();
                    // }
                });
                window.location.reload();

            },
            error: function (response){
                var errors = JSON.parse(response.responseText).errors;
                var errorsHtml = '<ul>';
                $.each(errors, function (index, value) {
                    errorsHtml += '<li><p class="text-danger">' + value + '</p></li>';
                });
                errorsHtml +='</ul>';
                $('#errorAlert').css('display','block');
                $('#errorAlert').html(errorsHtml);

            }
        });
    })
</script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/jquery.inputmask.bundle.min.js"></script>
    <script src="{{asset('assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
    <script>
        let func = function (){
            $("#datepicker-default").datepicker({format:'yyyy-mm-dd'})
            $('.phone_number').inputmask('+\\9\\9899-999-99-99');
            $('#fio').on('input', function () {
                $(this).next().css('display', 'none');
            });
        }
        $(document).ready(function () {
            func();
            $('#phone').on('input', function () {
                $(this).next().css('display', 'none');
            });
            $('#email').on('input', function () {
                $(this).next().css('display', 'none');
            });
            $('#newpassword').on('input', function () {
                $(this).next().css('display', 'none');
            });
            $('#birthday').on('change', function () {
                $(this).next().css('display', 'none');
            });
            $('#status').on('change', function () {
                $(this).next().css('display', 'none');
            });
            $('#role').on('change', function () {
                $(this).next().css('display', 'none');
            });
        });
    </script>
</body>
</html>
