@extends('layouts.auth')

@section('content')
    <!-- begin login -->
    <div class="login login-v2" data-pageload-addclass="animated fadeIn">
        <!-- begin brand -->
        <div class="login-header" style="text-align: center;">
            <div class="brand">
                <img @if(file_exists(public_path('images/logo.png'))) src="{{asset('images/logo.png')}}"
                     style="max-width: 80px; width: auto; max-height: 80%" @else src="{{asset('img/noimg.jpg')}}"
                     style="max-width: 30px; max-height: 30px; border-radius: 50%;" @endif />
                <small>CSPU marketing</small>
            </div>
        </div>
        <!-- end brand -->
        <div class="login-content">
            <form action="index.html" method="POST" class="margin-bottom-0">
                <div class="form-group m-b-20">
                    <input type="text" class="form-control input-lg" placeholder="Email Address" required/>
                </div>
                <div class="form-group m-b-20">
                    <input type="password" class="form-control input-lg" placeholder="Password" required/>
                </div>
                <div class="login-buttons">
                    <button type="submit" class="btn btn-success btn-block btn-lg">Sign me in</button>
                </div>
            </form>
        </div>
    </div>
    <!-- end login -->
    {{--    <!-- begin login -->--}}
    {{--    <div class="login bg-cover animated fadeInDown">--}}
    {{--        <!-- begin brand -->--}}
    {{--        <div class="login-header" style="text-align: center;">--}}
    {{--            <div class="brand" style="display: flex; flex-direction: column;">--}}
    {{--                <img @if(file_exists(public_path('images/logo.png'))) src="{{asset('images/logo.png')}}"--}}
    {{--                     style="max-width: 80px; width: auto; max-height: 80%" @else src="{{asset('img/noimg.jpg')}}"--}}
    {{--                     style="max-width: 30px; max-height: 30px; border-radius: 50%;" @endif />--}}
    {{--                <b style="font-weight: 500;">CSPU marketing</b>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--        <!-- end brand -->--}}
    {{--        <div class="login-content" style="margin-top: 20px;">--}}
    {{--            <form action="{{route('login')}}" method="POST" class="margin-bottom-0">--}}
    {{--                @csrf--}}
    {{--                <div class="form-group m-b-20">--}}
    {{--                    <input type="text" name="p_seria" value="{{old('p_seria')}}"--}}
    {{--                           class="form-control input-lg inverse-mode no-border @if('p_seria') is-invalid @enderror"--}}
    {{--                           placeholder="Passport seria" required/>--}}
    {{--                </div>--}}
    {{--                <div class="form-group m-b-20">--}}
    {{--                    <input type="text" name="p_number"--}}
    {{--                           class="form-control input-lg inverse-mode no-border @if('p_number') is-invalid @enderror"--}}
    {{--                           placeholder="Passport raqam" required/>--}}
    {{--                </div>--}}
    {{--                <div class="login-buttons">--}}
    {{--                    <button type="submit" class="btn btn-success btn-block btn-lg">Kirish</button>--}}
    {{--                </div>--}}
    {{--            </form>--}}
    {{--        </div>--}}
    {{--    </div>--}}

    {{--    <!-- end login -->--}}

@endsection
@section('loginJs')
    <script>
        $(document).ready(function () {
            $('body').removeClass('bg-white');
        });
    </script>
@endsection
