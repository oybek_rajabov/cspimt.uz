<form action="#" id="actionCreateForm" class="form-horizontal">
    @csrf
    <div class="row">
        <div class="col-md-6">
            <div>
                <label for="name"> Nomi</label>
                <input type="text" id="name"  name="name" class="form-control">
                <div class="help-block"></div>
            </div>
        </div>
        <div class="col-md-6">
            <div>
                <label for="name"> Shifr</label>
                <input type="text" id="code"  name="code" class="form-control">
                <div class="help-block"></div>
            </div>
        </div>
        <div class="col-md-12">
            <div>
                <label for="name"> Fakultet</label>
                <select name="faculty_id" id="faculty_id" class="form-control">
                    <option value="" selected>Tanlang..</option>
                    @foreach(faculties() as $faculty)
                        <option value="{{$faculty->id}}">{{$faculty->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
    <div id="errorAlert" style="display: none;">

    </div>
</form>
