<form method="get" action="{{route('admin.specialities.index')}}">
    @csrf
    <th></th>
    <th></th>
    <th><input type="text" name="name"  value="{{ request('name') }}" class="form-control"></th>
    <th><input type="text" name="code"  value="{{ request('code') }}" class="form-control"></th>
    <th>
        <select name="facultyId" id="facultyId" class="form-control">
            <option value="" selected>Tanlang..</option>
            @foreach(faculties() as $faculty)
                <option value="{{$faculty->id}}">{{$faculty->name}}</option>
            @endforeach
        </select>
    </th>
    <th>
        <button type="submit" class="btn btn-warning btn-sm">
            <i class="fa fa-filter"></i> Qidirish
        </button>
    </th>
</form>
