<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersSeedeer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        DB::table('users')->insert([
            [
                'username' => 'admin',
                'email' => 'admin@gmail.com',
                'password' => Hash::make('@oybek.94'),
                'status' => 1,
                'fio' => 'Oybek Rajabov',
                'phone' => '+998935079721',
                'role_id' => 1
            ],
            [
                'username' => 'Sanjar',
                'email' => 'marketing@gmail.com',
                'password' => Hash::make('@mtmarketing'),
                'status' => 1,
                'fio' => 'Rahimov Sanjar',
                'phone' => '+998935079722',
                'role_id' => 2
            ],
            [
                'username' => 'e_marketing',
                'email' => 'e_marketing@gmail.com',
                'password' => Hash::make('@parol_mt'),
                'status' => 1,
                'fio' => 'Marketing xodimi',
                'phone' => '+998935079723',
                'role_id' => 3
            ]

        ]);

        $lastId = User::query()->orderBy('id','desc')->first();
        DB::statement('alter sequence users_id_seq restart with '.(intval($lastId->id)+1));
    }
}
