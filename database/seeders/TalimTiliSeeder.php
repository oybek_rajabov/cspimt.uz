<?php

namespace Database\Seeders;

use App\Models\TalimTili;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TalimTiliSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('talim_tili')->truncate();
        DB::table('talim_tili')->insert([
            [
                'name' => 'Ўзбек',
            ],
            [
                'name' => 'Рус',
            ],
            [
                'name' => 'Тожик',
            ],
            [
                'name' => 'Қозоқ',
            ]
        ]);

        $lastId = TalimTili::query()->orderBy('id','desc')->first();
        DB::statement('alter sequence talim_tili_id_seq restart with '.(intval($lastId->id)+1));
    }
}
