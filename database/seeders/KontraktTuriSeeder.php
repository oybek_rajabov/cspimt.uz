<?php

namespace Database\Seeders;

use App\Models\KontraktTuri;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KontraktTuriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kontrakt_turi')->truncate();
        DB::table('kontrakt_turi')->insert([
            [
                'name' => 'Базовий контракт',

            ],
            [
                'name' => 'Оширилган контракт',

            ],
            [
                'name' => 'Перевод контракт',

            ]

        ]);

        $lastId = KontraktTuri::query()->orderBy('id','desc')->first();
        DB::statement('alter sequence kontrakt_turi_id_seq restart with '.(intval($lastId->id)+1));
    }
}
