<?php

namespace Database\Seeders;

use App\Models\Language;
use App\Models\Speciality;
use App\Models\TalimTili;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RolesSeeder::class,
            FacultySeeder::class,
            UsersSeedeer::class,
            YonalishSeeder::class,
            TalimTuriSeeder::class,
            KontraktTuriSeeder::class,
            TalimTiliSeeder::class,
            StudentsSeeder::class
        ]);
    }
}
