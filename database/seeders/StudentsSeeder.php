<?php

namespace Database\Seeders;

use App\Models\Student;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StudentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('students')->truncate();
        DB::table('students')->insert([
            [
                'fio' => "Tojiyev Sardor Mamatovich",
                'telefon' => "+998900118120",
                'email' => "abc@gmail.com",
                'p_seria' => "AB",
                'p_number' => "2095076"
            ]

        ]);

        $lastId = Student::query()->orderBy('id','desc')->first();
        DB::statement('alter sequence students_id_seq restart with '.(intval($lastId->id)+1));
    }
}
