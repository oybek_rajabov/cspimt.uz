<?php

namespace Database\Seeders;

use App\Models\Faculty;
use App\Models\Translate;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FacultySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('faculties')->truncate();
        DB::table('faculties')->insert([
            [
                'id' => 1,
                'name' => 'Математика ва информатика',
            ],
            [
                'id' => 2,
                'name' => 'Физика ва кимё',
            ],
            [
                'id' => 3,
                'name' => 'Табиий фанлар',
            ],
            [
                'id' => 4,
                'name' => 'Гуманитар фанлар',
            ],
            [
                'id' => 5,
                'name' => 'Педагогика',
            ],
            [
                'id' => 6,
                'name' => 'Матабгача таьлим',
            ],
            [
                'id' => 7,
                'name' => 'Бошланғич таьли',
            ],
            [
                'id' => 8,
                'name' => 'Туризм',
            ],
            [
                'id' => 9,
                'name' => 'Спорт ва чақириққача ҳарьий таьлим',
            ],
            [
                'id' => 10,
                'name' => 'Саньатшунослик',
            ],
        ]);

        $lastId = Faculty::query()->orderBy('id','desc')->first();
        DB::statement('alter sequence faculties_id_seq restart with '.(intval($lastId->id)+1));
    }
}
