<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->truncate();
        DB::table('roles')->insert([
            [
                'key' => 'admin',
                'status' => 1,
                'name' => 'Admin'
            ],
            [
                'key' => 'marketing',
                'status' => 1,
                'name' => 'Marketing'
            ],
            [
                'key' => 'marketing_xodim',
                'status' => 1,
                'name' => 'Marketing xodimi'
            ]

        ]);

        $lastId = Role::query()->orderBy('id','desc')->first();
        DB::statement('alter sequence roles_id_seq restart with '.(intval($lastId->id)+1));
    }
}
