<?php

namespace Database\Seeders;

use App\Models\TalimTuri;
use App\Models\YonalishTalimTuri;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use League\CommonMark\Extension\TaskList\TaskListExtension;

class TalimTuriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('talim_turi')->truncate();
        DB::table('talim_turi')->insert([
            [
                'name' => 'Кундузги',
            ],
            [
                'name' => 'Кечки',
            ],
            [
                'name' => 'Сиртқи',
            ],
            [
                'name' => 'Махсус сиртқи',
            ],
            [
                'name' => '2-Олий таьлим',
            ],
            [
                'name' => 'Магистратура',
            ]
        ]);

        $lastId = TalimTuri::query()->orderBy('id','desc')->first();
        DB::statement('alter sequence talim_turi_id_seq restart with '.(intval($lastId->id)+1));
    }
}
