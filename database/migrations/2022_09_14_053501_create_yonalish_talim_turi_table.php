<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateYonalishTalimTuriTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('yonalish_talim_turi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->foreignId('talim_turi_id')->nullable()->constrained('talim_turi')->onDelete('cascade');
            $table->foreignId('yonalish_id')->nullable()->constrained('yonalish')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('yonalish_talim_turi');
    }
}
