<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->bigIncrements('id')->unique();
            $table->string('fio')->nullable();
            $table->string('telefon')->nullable();
            $table->string('email')->nullable();
            $table->string('image')->nullable();
            $table->string('p_seria')->nullable();
            $table->string('p_number')->nullable();
            $table->boolean('jins')->nullable();
            $table->integer('kursi')->nullable();
            $table->string('nationality')->nullable();
            $table->string('address')->nullable();
            $table->string('shart_n')->nullable();
            $table->string('a_id')->nullable();
            $table->integer('t_turi')->nullable();
            $table->integer('st_tayinlash')->nullable();
            $table->integer('stipendiya_t')->nullable();
            $table->double('ksumma')->default(0);
            $table->double('yangi_tolov')->default(0);
            $table->double('tolandi')->default(0);
            $table->double('umumiy_tolov')->default(0);
            $table->double('qarzdorlik')->default(0);
            $table->double('ortgan_pul')->default(0);
            $table->boolean('shartnoma')->nullable();
            $table->date('shartnoma_date')->nullable();
            $table->foreignId('talim_tili_id')->nullable()->constrained('talim_tili')->onDelete('cascade');
            $table->foreignId('kontrakt_turi_id')->nullable()->constrained('kontrakt_turi')->onDelete('cascade');
            $table->foreignId('talim_turi_id')->nullable()->constrained('talim_turi')->onDelete('cascade');
            $table->foreignId('yonalish_id')->nullable()->constrained('yonalish')->onDelete('cascade');
            $table->foreignId('faculty_id')->nullable()->constrained('faculties')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
