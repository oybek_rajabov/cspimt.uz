<?php

namespace App\Rules;

use App\Rules\PhoneNumber;

class StudentRequests extends \Illuminate\Foundation\Http\FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'telefon' => [new PhoneNumber(request()->id), 'required', 'string', 'nullable'],
            'fio' => 'required',
            'p_number' => 'required',
            'p_seria' => 'required',
            'faculty_id' => 'required',
            'speciality_id' => 'required',
            'talim_turi_id' => 'required',
            'talim_tili_id' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'telefon.required' => 'Telefon raqamni kiriting',
            'fio.required' => 'FIO ni kiriting',
            'p_number.required' => 'Passport raqam kiritilishi kerak',
            'p_seria.required' => 'Passport seriya kiritilishi kerak',
            'email.unique' => 'bu Email mavjud',
            'faculty_id.required' => 'Fakultet tanlanishi kerak',
            'speciality_id.required' => 'Yo\'nalish tanlanishi kerak',
            'talim_turi_id.required' => 'Ta\'lim shakli tanlanishi kerak',
            'talim_tili_id.required' => 'Ta\'lim tili tanlanishi kerak'
        ];
    }

}
