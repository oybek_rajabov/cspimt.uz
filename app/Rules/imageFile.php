<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class imageFile implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $fileName = '';
    public function __construct()
    {

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $formats = ['jpeg','jpg','png','bmp','svg'];
        $this->fileName = $value->getClientOriginalName();
        if(in_array($value->getClientOriginalExtension(),$formats) && $value->getSize() < 5000000)
            return true;
        else return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->fileName.__('message.document_format');
    }
}
