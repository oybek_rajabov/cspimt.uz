<?php

namespace App\Rules;

use App\Rules\PhoneNumber;

class StudentApiRequests extends \Illuminate\Foundation\Http\FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => [new PhoneNumber(), 'required', 'string'],
            'fio' => 'required',
            'p_number' => 'integer|nullable',
            'photo' => 'mimes:jpg,bmp,png,jpeg',
            'birthday' => ['date','date_format:Y-m-d','nullable'],
            'sex' => ['boolean','nullable'],
        ];
    }

    public function messages()
    {
        return [
            'phone.required' => 'Telefon raqamni kiriting',
            'fio.required' => 'FIO ni kiriting',
            'fio.p_number' => 'Passport raqam xato',
        ];
    }

}
