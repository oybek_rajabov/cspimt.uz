<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class documentFile implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $fileName = '';
    public function __construct()
    {

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $formats = ['txt','doc','docx','xls','xlsx','ppt','pptx','zip','rar','7zip','pdf'];
        $this->fileName = $value->getClientOriginalExtension();
        if(in_array($value->getClientOriginalExtension(),$formats) && $value->getSize() < 5000000)
            return true;
        else return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->fileName.__('message.document_format');
    }
}
