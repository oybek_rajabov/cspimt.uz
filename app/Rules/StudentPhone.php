<?php

namespace App\Rules;

use App\Models\Student;
use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class StudentPhone implements Rule
{

    protected $id;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($id = 0)
    {
        $this->id = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $student = Student::where('phone', '=', str_replace(['(',')','-',' '], "", $value))->first();
        if (!$student)
            return true;
        else
        {
            if($this->id > 0 && $student->id == $this->id)
                return true;
            else return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('Kiritilgan telefon raqam bazada mavjud');
    }
}
