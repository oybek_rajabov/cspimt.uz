<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class requiredData implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    protected $attrLabel;
    public function __construct($attrLabel)
    {
        $this->attrLabel = $attrLabel;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if($value == '')
            return false;
        else return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->attrLabel.' maydoni to\'ldirilishi shart.';
    }
}
