<?php

namespace App\Http\Collection;

use App\Models\Student;

class StudentCollection extends Student
{
    public function toArray()
    {
        return [
            'id'    => $this->id,
            'fio'    => $this->fio,
            'shart_n'   => $this->shart_n ?? '',
            'date' => date('d.m.Y'),
            'yonalish' => $this->speciality->name ?? '',
            'talim_turi' => $this->talimturi->name ?? '',
            'address' => $this->address ?? '',
            'p_seria' => $this->p_seria ?? '',
            'p_number' => $this->p_number ?? '',
            'ksumma' => $this->ksumma ?? 0
        ];
    }

}
