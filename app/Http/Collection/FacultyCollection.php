<?php

namespace App\Http\Collection;

use App\Models\Api\Faculty;

class FacultyCollection extends Faculty
{
    public function toArray()
    {
        return [
            'id'         => $this->id,
            'key'        => $this->key,
            'name'       => $this->getName(),
            'speciality' => $this->specialityList(),
        ];
    }

    /**
     * @return array
     */
    public function specialityList()
    {
        $result = [];
        foreach ($this->speciality as $value) {
            $result[] = [
                'id'    => $value->id,
                'key'   => $value->key,
                'name'  => $value->getName(),
                'group' => self::getGroupList($value->group)
            ];
        }
        return $result;
    }


    /**
     * @param $datas
     * @return array
     */
    public static function getGroupList($datas)
    {
        $result = [];
        foreach ($datas as $value) {
            $result[] = [
                'id'    => $value->id,
                'course'   => $value->course,
                'name'  => $value->getName(),
                'description'  => $value->getText(),
            ];
        }

        return $result;
    }

}
