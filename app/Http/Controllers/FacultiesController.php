<?php

namespace App\Http\Controllers;

use App\Http\Filters\FacultyFilter;
use App\Models\Faculty;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class FacultiesController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index(FacultyFilter $filter)
    {
        $datas = Faculty::filter($filter)
            ->sortable()
            ->paginate(15);
        return  view('faculties.index',compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create()
    {
        $header = '<h5 class="modal-title" id="createActionLabel">Qo\'shish</h5>';
        $footer = '<button type="button" class="btn btn-sm btn-white"
                                data-dismiss="modal">Yopish</button>
                        <a href="#" id="createSubmit" class="btn btn-sm btn-success">Saqlash</a>';
        $view = view('faculties.create');
        return ['header' => $header, 'content' => $view->render(), 'footer' => $footer];
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required'
        ],[
            'name.required' => 'Nomi maydoni to\'ldirilishi shart'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validator->errors(),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

            $faculty = new Faculty();
            $faculty->name = $request->name;
            $faculty->key = Str::slug($request->name);
            $faculty->save();
            return ['message' => 'Fakultet yaratildi'];
    }

    public function edit($id)
    {
        $faculty = Faculty::where('id', $id)->first();
        $header = '<h5 class="modal-title" id="createActionLabel">Qo\'shish</h5>';
        $footer = '<button type="button" class="btn btn-sm btn-white"
                                data-dismiss="modal">Yopish</button>
                        <a href="#" onclick="updateFaculty('.$id.')" class="btn btn-sm btn-success">Saqlash</a>';
        $view = view('faculties.edit',compact('faculty'));
        return ['header' => $header, 'content' => $view->render(), 'footer' => $footer];
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Faculty  $faculty
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        if($data = $this->validate($request,[
            'name' => 'required'
        ],[
            'name.required' => 'Nomi maydoni kiritilishi shart!'
        ])){
            $faculty = Faculty::where('id',$id)->first();
            $faculty->name = $data['name'];
            $faculty->key = Str::slug($data['name']);
            $faculty->save();
            return ['message' => 'Fakultet tahrirlandi'];
        }
    }


    /*
       * education type delete
       */
    public function delete($id)
    {
        $faculty = Faculty::where('id', $id)->first();
        $header = '<h5 class="modal-title" id="createActionLabel">Tasdiqlang</h5>';
        $footer = '<button type="button" class="btn btn-sm btn-white pull-left"
                                data-dismiss="modal">Yopish</button>
                        <a href="#" onclick="deleteFaculty('.$faculty->id.')"class="btn btn-sm btn-danger" style="margin: 2px;">
            Ha</a>';
        $content = '<h4>Rostdan ham o\'chirishni xohlaysizmi?</h4>';
        return ['header' => $header, 'content' => $content, 'footer' => $footer];
    }

    public function destroy($id)
    {
        $model = Faculty::where('id', $id)->first();
        if ($model) {
            $model->delete();
        }
        return back()->withSuccess(__("message.data_deleted"));
    }
}
