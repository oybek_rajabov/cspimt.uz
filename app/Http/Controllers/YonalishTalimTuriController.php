<?php

namespace App\Http\Controllers;

use App\Http\Filters\YonalishTalimTuriFilter;
use App\Models\TalimTuri;
use App\Models\YonalishTalimTuri;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class YonalishTalimTuriController extends Controller
{
    public function index(YonalishTalimTuriFilter $filter)
    {
        $datas = YonalishTalimTuri::with('speciality')
            ->with('talimturi')
            ->filter($filter)
            ->paginate(15);
        return  view('yonalishtalimturi.index',compact('datas'));
    }

    public function create()
    {
        $header = '<h5 class="modal-title" id="createActionLabel">Qo\'shish</h5>';
        $footer = '<button type="button" class="btn btn-sm btn-white"
                                data-dismiss="modal">Yopish</button>
                        <a href="#" id="createSubmit" class="btn btn-sm btn-success">Saqlash</a>';
        $view = view('yonalishtalimturi.create');
        return ['header' => $header, 'content' => $view->render(), 'footer' => $footer];
    }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'talimturi_id' => 'required',
            'speciality_id' => 'required'
        ],[
            'talimturi_id.required' => 'Talim shakli maydoni kiritilishi shart!',
            'speciality_id.required' => 'Yo\'nalish maydoni kiritilishi shart!'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validator->errors(),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }


        $model = YonalishTalimTuri::query()->where('talim_turi_id', $request->talimturi_id)
            ->where('yonalish_id', $request->speciality_id)->first();
        if($model === null){
            $yonalishtalimturi = new YonalishTalimTuri();
            $yonalishtalimturi->talim_turi_id = $request->talimturi_id;
            $yonalishtalimturi->yonalish_id = $request->speciality_id;
            $yonalishtalimturi->save();
            return ['success' => 'Yonalish ta\'lim turi yaratildi'];
        }
        return ['success' => 'Bu yozuv bazada mavjud'];

    }

    public function edit($id)
    {
        $yonalishtalimturi = YonalishTalimTuri::where('id', $id)->first();
        $header = '<h5 class="modal-title" id="createActionLabel">Qo\'shish</h5>';
        $footer = '<button type="button" class="btn btn-sm btn-white"
                                data-dismiss="modal">Yopish</button>
                        <a href="#" onclick="updateYonalishTalimTuri('.$id.')" class="btn btn-sm btn-success">Saqlash</a>';
        $view = view('yonalishtalimturi.edit',compact('yonalishtalimturi'));
        return ['header' => $header, 'content' => $view->render(), 'footer' => $footer];
    }

    public function update(Request $request, $id)
    {
        if($data = $this->validate($request,[
            'talimturi_id' => 'required',
            'speciality_id' => 'required'
        ],[
            'talimturi_id.required' => 'Talim shakli maydoni kiritilishi shart!',
            'speciality_id.required' => 'Yo\'nalish maydoni kiritilishi shart!'
        ])){
            $model = YonalishTalimTuri::query()->where('id','!=',$id)
                ->where('talim_turi_id', $request->talimturi_id)
                ->where('yonalish_id', $request->speciality_id)->first();
            if($model === null){
                $yonalishtalimturi = YonalishTalimTuri::where('id',$id)->first();
                $yonalishtalimturi->talim_turi_id = $request->talimturi_id;
                $yonalishtalimturi->yonalish_id = $request->speciality_id;
                $yonalishtalimturi->save();
                return ['success' => 'Yonalish talim turi shakli tahrirlandi'];
            }
            return ['success' => 'Bu yozuv bazada mavjud'];

        }
    }

    public function delete($id)
    {
        $yonalishtalimturi = YonalishTalimTuri::where('id', $id)->first();
        $header = '<h5 class="modal-title" id="createActionLabel">Tasdiqlang</h5>';
        $footer = '<button type="button" class="btn btn-sm btn-white pull-left"
                                data-dismiss="modal">Yopish</button>
                        <a href="#" onclick="deleteYonalishTalimTuri('.$yonalishtalimturi->id.')"class="btn btn-sm btn-danger" style="margin: 2px;">
            Ha</a>';
        $content = '<h4>Rostdan ham o\'chirishni xohlaysizmi?</h4>';
        return ['header' => $header, 'content' => $content, 'footer' => $footer];
    }

    public function destroy($id)
    {
        $model = YonalishTalimTuri::where('id', $id)->first();
        if ($model) {
            $model->delete();
        }
        return back()->withSuccess(__("message.data_deleted"));
    }

}
