<?php

namespace App\Http\Controllers;

use App\Models\TalimTili;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class TalimTiliController extends Controller
{
    public function index(Request $request)
    {
        $datas = TalimTili::paginate(15);
        return  view('talimtili.index',compact('datas'));
    }

    public function create()
    {
        $header = '<h5 class="modal-title" id="createActionLabel">Qo\'shish</h5>';
        $footer = '<button type="button" class="btn btn-sm btn-white"
                                data-dismiss="modal">Yopish</button>
                        <a href="#" id="createSubmit" class="btn btn-sm btn-success">Saqlash</a>';
        $view = view('talimtili.create');
        return ['header' => $header, 'content' => $view->render(), 'footer' => $footer];
    }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required'
        ],[
            'name.required' => 'Nomi maydoni to\'ldirilishi shart'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validator->errors(),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $talimtili = new TalimTili();
        $talimtili->name = $request->name;
        $talimtili->save();
        return ['message' => 'Ta\'lim tili yaratildi'];
    }

    public function edit($id)
    {
        $talimtili = TalimTili::where('id', $id)->first();
        $header = '<h5 class="modal-title" id="createActionLabel">Qo\'shish</h5>';
        $footer = '<button type="button" class="btn btn-sm btn-white"
                                data-dismiss="modal">Yopish</button>
                        <a href="#" onclick="updateTalimTili('.$id.')" class="btn btn-sm btn-success">Saqlash</a>';
        $view = view('talimtili.edit',compact('talimtili'));
        return ['header' => $header, 'content' => $view->render(), 'footer' => $footer];
    }

    public function update(Request $request, $id)
    {
        if($data = $this->validate($request,[
            'name' => 'required'
        ],[
            'name.required' => 'Nomi maydoni kiritilishi shart!'
        ])){
            $talimtili = TalimTili::where('id',$id)->first();
            $talimtili->name = $data['name'];
            $talimtili->save();
            return ['message' => 'Talim shakli tahrirlandi'];
        }
    }

    public function delete($id)
    {
        $talimtili = TalimTili::where('id', $id)->first();
        $header = '<h5 class="modal-title" id="createActionLabel">Tasdiqlang</h5>';
        $footer = '<button type="button" class="btn btn-sm btn-white pull-left"
                                data-dismiss="modal">Yopish</button>
                        <a href="#" onclick="deleteTalimTili('.$talimtili->id.')"class="btn btn-sm btn-danger" style="margin: 2px;">
            Ha</a>';
        $content = '<h4>Rostdan ham o\'chirishni xohlaysizmi?</h4>';
        return ['header' => $header, 'content' => $content, 'footer' => $footer];
    }

    public function destroy($id)
    {
        $model = TalimTili::where('id', $id)->first();
        if ($model) {
            $model->delete();
        }
        return back()->withSuccess(__("message.data_deleted"));
    }

}
