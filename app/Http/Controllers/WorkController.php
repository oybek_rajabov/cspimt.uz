<?php

namespace App\Http\Controllers;

class WorkController extends Controller
{
    public function index()
    {
        return view('work.index');
    }

    public function studentInterance()
    {
        return view('work.interance');
    }
}
