<?php

namespace App\Http\Controllers;

use App\Http\Filters\UsersFilter;
use App\Models\Faculty;
use App\Models\Role;
use App\Models\User;
use App\Rules\PhoneNumber;
use App\Rules\UserNameUnique;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(UsersFilter $request)
    {
        $users = User::filter($request)
            ->with('role')
            ->sortable()
            ->paginate(15);
        $roles = Role::all();
        return view('users.index', compact('users', 'roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        $header = '<h5 class="modal-title" id="createActionLabel">Qo\'shish</h5>';
        $footer = '<button type="button" class="btn btn-sm btn-white"
                                data-dismiss="modal">Yopish</button>
                        <a href="#" id="createSubmit" class="btn btn-sm btn-success">Saqlash</a>';
        $view = \view('users.create', compact('roles'));
        return ['header' => $header, 'content' => $view->render(), 'footer' => $footer];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        if ($data = $request->validate([
            'fio' => 'required',
            'username' => 'required|min:6|unique:users,username',
            'password' => 'required|string|min:6|max:20',
            'role' => 'required',
            'phone' => ['nullable', new PhoneNumber()]
        ], [
            'fio.required' => 'FIO maydoni kiritilishi shart.',
            'username.required' => 'Login maydoni kiritilishi shart.',
            'username.min' => 'Login kamida 6 ta belgidan iborat bo\'lishi shart.',
            'username.unique' => 'Kiritilgan Login bazada mavjud.',
            'password.required' => 'Parol kiritilishi shart.',
            'password.min' => 'Parol kamida 6 ta belgidan iborat bo\'lishi shart.',
            'password.max' => 'Parol  20 ta belgidan oshmasligi kerak.',
            'role.required' => 'Rol maydoni to\'ldirilishi shart'
        ])) {
            $user = new User();
            $user->username = $data['username'];
            $user->password = Hash::make($data['password']);
            $user->fio = $data['fio'];
            $user->role_id = $data['role'];
            $user->phone = $data['phone'] ? '+'.str_replace(['(', ')', '-', '+', ' '], "", $data['phone']) : null;
            $user->save();
            return \response()->json(['success' => 'Ma\'lumot saqlandi']);
        } else return \response()->json(['error' => 'Xatolik']);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $user = User::where('id', $user->id)->with(['role', 'faculty'])->first();
        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $user = User::where('id', $user->id)->with('role')->first();
        $roles = Role::all();
        return view('users.update', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        if ($data = $this->validateUpdateForm($request, $user)) {
            $user->fio = $data['fio'];
            $user->username = $data['username'];
            $user->status = $data['status'];
            if ($request->role) ;
            $user->role_id = $request->role;
            $user->phone = $data['phone'] ? str_replace(['(', ')', '-', ' '], "", $data['phone']) : null;
            if (!$request->status === null)
                $user->status = $request->status;
            $user->save();
            if ($data['newpassword'])
                $user->password = Hash::make($data['newpassword']);
            if ($request->hasFile('image')) {
                if ($user->image) {
                    if (file_exists(storage_path() . '/app/public/users/' . $user->image))
                        unlink(storage_path() . '/app/public/users/' . $user->image);
                }
                $file = $request->file('image');
                $file_name = time() .'.jpg';
                $file->storeAs('public/' . 'users', $file_name);
                $user->image = $file_name;
                $user->save();
            }

            $user->save();
            return redirect()->route('users.index');
        } else redirect()->back()->with('error', 'Formani to\'ldirishda xatolik bor');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        try {
            $user->delete();
            return true;
        } catch (\Exception $e) {
            return ['message' => $e];
        }
    }

    public function validateUpdateForm($request, $user)
    {
        if ($data = $request->validate([
            'fio' => 'required',
            'username' => ['required', 'min:6', new UserNameUnique($user->id)],
            'newpassword' => 'nullable|string|min:6|max:20',
            'role' => 'required',
            'phone' => ['nullable', new PhoneNumber($user->id)],
            'image' => 'nullable|mimes:jpeg,jpg,png,bmp',
            'status' => ''
        ], [
            'fio.required' => 'FIO maydoni kiritilishi shart.',
            'username.required' => 'Login maydoni kiritilishi shart.',
            'username.min' => 'Login kamida 6 ta belgidan iborat bo\'lishi kerak.',
            'newpassword.required' => 'Parol kiritilishi shart.',
            'newpassword.min' => 'Parol kamida 6 ta belgidan iborat bo\'lishi shart.',
            'newpassword.max' => 'Parol  20 ta belgidan oshmasligi kerak.',
            'role.required' => 'Rol maydoni to\'ldirilishi shart',
        ])) {
            return $data;
        } else return false;
    }

    /**
     * @param Request $request
     * @param $folder
     * @param $file
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * file download
     */
    public function download(Request $request, $folder, $fileName)
    {
        $path = storage_path() . '/app/public/users/' . $folder . '/' . $fileName;
        if (file_exists($path))
            return response()->download($path);
        else redirect()->back();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Activity $activity
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, User $user)
    {
        if ($request->isMethod('GET')) {
            $header = '<h5 class="modal-title" id="createActionLabel">Tasdiqlang</h5>';
            $footer = '<button type="button" class="btn btn-sm btn-white pull-left"
                                data-dismiss="modal">Yopish</button>
                        <a href="#" onclick="deleteUser(' . $user->id . ')"class="btn btn-sm btn-danger" style="margin: 2px;">
            Ha</a>';
            $content = '<h4>Rostdan ham o\'chirishni xohlaysizmi?</h4>';
            return ['header' => $header, 'content' => $content, 'footer' => $footer];
        }
        if ($request->isMethod('DELETE')) {
            if ($user) {
                $user->delete();
                return true;
            } else
                return false;
        }

    }


    /**
     * @param User $user
     * @return array
     * show profile
     */
    public function profileShow(User $user)
    {
        $roles = Role::all();
        $faculties = Faculty::all();
        $header = '<h5 class="modal-title" id="updateActionLabel">Shaxsiy kabinet</h5>';
        $footer = '<button type="button" class="btn btn-sm btn-white"
                                data-dismiss="modal">Yopish</button>
                        <button type="submit" id="profileUpdateSubmit" class="btn btn-sm btn-success">Saqlash</button>';
        $view = \view('users.profile-show', compact('user','roles', 'faculties'));
        return ['header' => $header, 'content' => $view->render(), 'footer' => $footer];

    }


    /**
     * @param User $user
     * @return array
     * update Profile User
     */
    public function profileUpdate(Request $request)
    {
        $user = User::where('id', auth()->user()->id)->first();
        if ($data = $request->validate([
            'fio' => 'required',
            'username' => ['required', 'min:6', new UserNameUnique($user->id)],
            'newpassword' => 'nullable|string|min:6|max:20',
            'phone' => ['nullable', new PhoneNumber($user->id)],
            'image' => 'nullable|mimes:jpeg,jpg,png,bmp',
        ], [
            'fio.required' => 'FIO maydoni kiritilishi shart.',
            'username.required' => 'Login maydoni kiritilishi shart.',
            'username.min' => 'Login kamida 6 ta belgidan iborat bo\'lishi kerak.',
            'newpassword.required' => 'Parol kiritilishi shart.',
            'newpassword.min' => 'Parol kamida 6 ta belgidan iborat bo\'lishi shart.',
            'newpassword.max' => 'Parol  20 ta belgidan oshmasligi kerak.',
        ])) {
            $user->fio = $data['fio'];
            $user->username = $data['username'];
            if ($request->faculty) ;
            $user->faculty_id = $request->faculty;
            $user->sex = $request->sex;
            if ($request->birthday) {
                $user->birthday = date('Y-m-d', strtotime($request->birthday));
            }
            $user->phone = $data['phone'] ? str_replace(['(', ')', '-', ' '], "", $data['phone']) : null;
            if (!$request->status === null)
                $user->status = $request->status;
            $user->save();
            if ($data['newpassword'])
                $user->password = Hash::make($data['newpassword']);
            if ($request->hasFile('image')) {
                if ($user->image) {
                    if (file_exists(storage_path() . '/app/public/users/' . $user->image))
                        unlink(storage_path() . '/app/public/users/' . $user->image);
                }
                $file = $request->file('image');
                $file_name = time() . $file->getClientOriginalName();
                $file->storeAs('public/' . 'users', $file_name);
                $user->image = $file_name;
                $user->save();
            }

            $user->save();
            return true;
        } else return false;

    }

}
