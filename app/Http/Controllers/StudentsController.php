<?php

namespace App\Http\Controllers;

use App\Http\Filters\StudentFilter;
use App\Imports\StudentsImport;
use App\Models\Faculty;
use App\Models\KontraktTuri;
use App\Models\Speciality;
use App\Models\Student;
use App\Models\TalimTili;
use App\Models\TalimTuri;
use App\Rules\StudentRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

use Mpdf\Mpdf;
use Symfony\Component\HttpFoundation\Response;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(StudentFilter $request)
    {
        $faculties = faculties();
        $specialities = specialities();
        $talimturlari = talimturlari();
        $students = Student::with('speciality')
            ->with('faculty')
            ->filter($request)
            ->sortable()
            ->orderBy('id', 'desc')
            ->paginate(50);
        return view('students.index', compact('students', 'talimturlari', 'faculties', 'specialities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        $faculties = Faculty::orderBy('id', 'asc')->get();
        $talimtillari = TalimTili::orderBy('id', 'asc')->get();
        $talimturlari = TalimTuri::orderBy('id', 'asc')->get();
        $kontraktturlari = KontraktTuri::orderBy('id', 'asc')->get();
        return view('students.create', compact(
            'faculties',
            'talimtillari',
            'talimturlari',
            'kontraktturlari'
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentRequests $request)
    {
        $model = new Student();
        $model->saveData($request->all());
        return redirect()->route('students.index');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Student $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $model)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Student $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $model)
    {
        $faculties = Faculty::orderBy('id', 'asc')->get();
        $talimtillari = TalimTili::orderBy('id', 'asc')->get();
        $talimturlari = TalimTuri::orderBy('id', 'asc')->get();
        $kontraktturlari = KontraktTuri::orderBy('id', 'asc')->get();
        return view('students.edit', compact('faculties',
            'talimturlari',
            'talimtillari',
            'kontraktturlari',
            'model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Student $student
     * @return \Illuminate\Http\Response
     */
    public function update(StudentRequests $request, Student $model)
    {
        $model->saveData($request->all());
        return redirect()->route('students.index');
    }


    public function getSpecialty(Request $request)
    {
        $datas = Speciality::where('faculty_id', $request->id)->get();
        $result [] = '<option value="">Tanlang</option>';
        foreach ($datas as $value) {
            $result [] = '<option value="' . $value->id . '">' . $value->name . '</option>';
        }
        return $result;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Activity $activity
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, Student $student)
    {
        if ($request->isMethod('GET')) {
            $header = '<h5 class="modal-title" id="createActionLabel">Tasdiqlang</h5>';
            $footer = '<button type="button" class="btn btn-sm btn-white pull-left"
                                data-dismiss="modal">Yopish</button>
                        <a href="#" onclick="deleteStudent(' . $student->id . ')"class="btn btn-sm btn-danger" style="margin: 2px;">
            Ha</a>';
            $content = '<h4>Rostdan ham o\'chirishni xohlaysizmi?</h4>';
            return ['header' => $header, 'content' => $content, 'footer' => $footer];
        }
        if ($request->isMethod('DELETE')) {
            if ($student) {
                $student->delete();
                return true;
            } else
                return false;
        }

    }

    public function importStudents(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'students_file' => 'mimes:xls,xlsx|required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validator->errors(),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        $array = Excel::toArray(new StudentsImport(), request()->file('students_file'));
//        $excel_validate = $array[0][0] ?? [];
//        if (!(isset($excel_validate[1]) && $excel_validate[1] == 'Фамиля, Исми ва Отасининг исми' &&
//            isset($excel_validate[2]) && $excel_validate[2] == 'Паспорт серия' &&
//            isset($excel_validate[3]) && $excel_validate[3] == 'Паспорт номери' &&
//            isset($excel_validate[4]) && $excel_validate[4] == 'Доимий яшаш манзили' &&
//            isset($excel_validate[5]) && $excel_validate[5] == 'Таълим шакли' &&
//            isset($excel_validate[7]) && $excel_validate[7] == 'Таълим тили' &&
//            isset($excel_validate[7]) && $excel_validate[7] == 'контракт турлари (базовый, оширилган ва перевод)' &&
//            isset($excel_validate[8]) && $excel_validate[8] == 'СУММАСИ' &&
//            isset($excel_validate[11]) && $excel_validate[11] == 'курс' &&
//            isset($excel_validate[12]) && $excel_validate[12] == 'ID')) {
//
//            return response()->json([
//                'error' => true,
//                'message' => 'Faylda berilgan ma\'lumotlar shablon bo\'yicha emas!',
//            ], Response::HTTP_UNPROCESSABLE_ENTITY);
//        }

        foreach ($array[0] as $key => $value) {
            if ($key != 0) {
                $speciality = Speciality::where('code', $value[6])->first() ?? null;
                if ($speciality) {
                    $yonalish_id = $speciality->id;
                    $faculty_id = $speciality->faculty_id;
                } else {
                    $yonalish_id = null;
                    $faculty_id = null;
                }
                $result[] = [
                    'shart_n' => trim($value[0]) ?? '',
                    'fio' => trim($value[1]) ?? '',
                    'p_seria' => trim($value[2]) ?? '',
                    'p_number' => trim($value[3]) ?? '',
                    'address' => $value[4] ?? '',
                    'talim_turi_id' => $value[5] ?? null,
                    'yonalish_id' => $yonalish_id  ?? null,
                    'faculty_id' => $faculty_id  ?? null,
                    'talim_tili_id' => $value[7] ?? null,
                    'kontrakt_turi_id' => $value[8] ?? null,
                    'ksumma' => $value[9]  ?? 0,
                    'stipendiya_t' => intval($value[10]) ?? null,
                    'kursi' => $value[11] ?? null,
                    'a_id' => trim($value[12]) ?? '',
                    'telefon' => trim($value[13]) ?? ''
                ];
            }
        }
        $k = 0;
        foreach (array_chunk($result, 500) as $t) {
            $k++;
            Student::insert($t);
        }

        if ($k == 0) {
            $file = [
                'error' => ['Soxta maʼlumotlarni import qilib boʻlmaydi!']
            ];
            return response()->json([
                'error' => true,
                'message' => $file,
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }
        return true;

    }

    public function downloadExcelTemplate(Request $request)
    {
        $path = public_path('assets/template/Talaba_list.xlsx');
        if (file_exists($path))
            return response()->download($path);
        else redirect()->route('students.index');
    }

    public function deleteAll(Request $request)
    {
        $ids = explode(',', $request->ids);
        foreach ($ids as $id) {
            if ($student = Student::where('id', $id)->first()) {
                $student->delete();
            }
        }
        return ['success' => 'Tanlanganlar o\'chirildi!'];
    }

    public function shartnomaTuri(Request $request)
    {
        $ids = $request->ids;
        $header = '<h5 class="modal-title" id="createActionLabel">Shartnomaga</h5>';
        $footer = '<button type="button" class="btn btn-sm btn-white"
                                data-dismiss="modal">Yopish</button>
                        <button type="submit" class="btn btn-sm btn-success">Yuklash</button></form>';
        $view = view('students.shartnomaga',compact('ids'));
        return ['header' => $header, 'content' => $view->render(), 'footer' => $footer];
    }

    /**
     * @throws \Mpdf\MpdfException
     * @throws \setasign\Fpdi\PdfParser\CrossReference\CrossReferenceException
     * @throws \setasign\Fpdi\PdfParser\PdfParserException
     * @throws \setasign\Fpdi\PdfParser\Type\PdfTypeException
     */
    public function shartnomaga(Request $request)
    {
        $ids = explode(',',$request->ids);
        $date = $request->date ?? date('Y-m-d');
        $models = Student::query()->whereIn('id', $ids)->with('faculty')->get();
        $mpdf = new Mpdf([
            'mode' => 'utf-8',
            'orientation' => 'L',
            'default_font_size' => 9
        ]);

        foreach ($models as $model){
            if (1) {
                $mpdf->SetSourceFile(public_path('documents/ikki_stipendiyasiz.pdf'));
                $template = $mpdf->ImportPage(1);
                $mpdf->UseTemplate($template);
                $logo = '<div style="position: absolute; top: 10px; left: 25px"><img src="'.public_path('images/logo.png').'" width="70px" height="70px"></div>';
                $mpdf->WriteHTML($logo);
                $shartnomaRaqami = $model->shart_n ?? '';
                $shart_n1 = '<div style="position: absolute; font-weight: bold; top: 66px; left: 300px">' .$shartnomaRaqami. '</div>';
                $mpdf->WriteHTML($shart_n1);
                $kun1 = '<div style="position: absolute; top:80px; left: 465px">' . $date . '</div>';
                $mpdf->WriteHTML($kun1);
                $fio1 = '<div style="position: absolute; font-weight: bold; top: 227px; left: 70px">' . $model->fio . '</div>';
                $mpdf->WriteHTML($fio1);
                $yonalish_name = $model->speciality->name ?? '';
                $yonalish = '<div style="position: absolute; font-weight: bold; top: 255px; left: 50px">' . $yonalish_name . '</div>';
                $mpdf->WriteHTML($yonalish);
                $talim_turi_name = $model->talimturi->name ?? '';
                $t_shakl = '<div style="position: absolute; font-weight: bold; top: 283px; left: 32px">' . $talim_turi_name . '</div>';
                $mpdf->WriteHTML($t_shakl);
                $fio2 = '<div style="position: absolute; font-weight: bold; font-size: 11px; top: 562px; left: 650px">' . $model->fio . '</div>';
                $mpdf->WriteHTML($fio2);
                $manzil = '<div style="position: absolute; font-size: 11px; top: 577px; left: 700px">' . $model->address . '</div>';
                $mpdf->WriteHTML($manzil);
//                $tel = '<div style="position: absolute; font-size: 11px; top: 689px; left: 912px">' . $model->telefon . '</div>';
//                $mpdf->WriteHTML($tel);
                $pasport_seriya = '<div style="position: absolute; font-weight: bold;  font-size: 11px; top: 594px; left: 705px">' . $model->p_seria . '&nbsp;</div>';
                $mpdf->WriteHTML($pasport_seriya);
                $pasport_raqam = '<div style="position: absolute; font-weight: bold; font-size: 11px; top: 594px; left: 788px">' . $model->p_number . '</div>';
                $mpdf->WriteHTML($pasport_raqam);
                $shart_n2 = '<div style="position: absolute; font-weight: bold; top: 510px; left: 730px">' . $shartnomaRaqami . '</div>';
                $mpdf->WriteHTML($shart_n2);
                $kun2 = '<div style="position: absolute; top: 510px; left: 600px">' . $date . '</div>';
                $mpdf->WriteHTML($kun2);
                $ksumma = '<div style="position: absolute; font-weight: bold; font-size: 16px; top: 680px; left: 440px">' . $model->ksumma . '</div>';
                $mpdf->WriteHTML($ksumma);
//                $mpdf->WriteHTML($shart_n2);
//                $foi = explode(' ', $model->fio);
//                if (count($foi) > 2)
//                    $fio3 = '<div style="position: absolute; font-weight: bold; font-size: 11px; top: 661px; left: 840px">' . $foi[0] . ' ' . mb_substr($foi[1], 0, 1) . '.' . mb_substr($foi[2], 0, 1) . '</div>';
//                else
//                    $fio3 = '<div style="position: absolute; font-weight: bold; font-size: 11px; top: 661px; left: 840px">' . $model->fio . '</div>';
//                $mpdf->WriteHTML($fio3);
                $mpdf->AddPage();
            }
        }

        $blankpage = $mpdf->page + 1;
        $mpdf->DeletePages($blankpage);
        $mpdf->Output();
    }

}
