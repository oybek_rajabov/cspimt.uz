<?php

namespace App\Http\Controllers\Api;

use App\Traits\ApiResponse;

class ApiController extends \App\Http\Controllers\Controller
{
    use ApiResponse;
}
