<?php

namespace App\Http\Controllers\Api\Student;

use App\Http\Collection\FacultyCollection;
use App\Http\Collection\StudentCollection;
use App\Http\Controllers\Api\ApiController;
use App\Models\Student;
use Illuminate\Http\Request;

class StudentController extends ApiController
{

    /*
     * student list
     */
    public function getModel(Request $request)
    {
        if($request->status == 1){
            $p_seria = $request->p_seria;
            $p_number = $request->p_number;
            $student = StudentCollection::where('p_seria', $p_seria)
                ->where('p_number', $p_number)
                ->with('speciality')
                ->with('talimturi')->first();
        }
        if($request->status == 2){
            $p_seria = $request->p_seria;
            $student = StudentCollection::where('telefon', $p_seria)
                ->OrWhere('telefon', '+'.$p_seria)
                ->with('speciality')
                ->with('talimturi')->first();
        }
        if($student)
            return $this->successResponse($student);
       else return $this->errorResponse("Ma'lumot topilmadi.");
    }

}
