<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\ApiController;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class LoginController extends ApiController
{

    /*
     * login for api
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => ['required', 'string'],
            'password' => ['required'],
        ]);

        if ($validator->fails()) {
            return $this->errorResponse($validator->errors());
        }

        $user = User::where('username', request(['username']))
            ->first();

        if (!$user || ! Hash::check($request->password, $user->password)) {
            return $this->errorResponse(__('username xato'), Response::HTTP_BAD_REQUEST);
        }

        elseif(Auth::login($user)) {
            return $this->errorResponse(__('tizimga kirdi'), Response::HTTP_UNAUTHORIZED);
        }
        return $this->successResponse($this->tokenWithSanctum());
    }


    /**
     * @return array
     * create Token
     */
    public function tokenWithSanctum(): array
    {
        $data = [
            'access_token' => auth()->user()->createToken('API Token')->plainTextToken,
            'token_type' => 'bearer',
        ];
        return $data;
    }
}
