<?php

namespace App\Http\Controllers;

use App\Models\KontraktTuri;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class KontrakTuriController extends Controller
{
    public function index(Request $request)
    {
        $datas = KontraktTuri::paginate(15);
        return  view('kontraktturi.index',compact('datas'));
    }

    public function create()
    {
        $header = '<h5 class="modal-title" id="createActionLabel">Qo\'shish</h5>';
        $footer = '<button type="button" class="btn btn-sm btn-white"
                                data-dismiss="modal">Yopish</button>
                        <a href="#" id="createSubmit" class="btn btn-sm btn-success">Saqlash</a>';
        $view = view('kontraktturi.create');
        return ['header' => $header, 'content' => $view->render(), 'footer' => $footer];
    }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required'
        ],[
            'name.required' => 'Nomi maydoni to\'ldirilishi shart'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validator->errors(),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $KontraktTuri = new KontraktTuri();
        $KontraktTuri->name = $request->name;
        $KontraktTuri->save();
        return ['message' => 'Ta\'lim shakli yaratildi'];
    }

    public function edit($id)
    {
        $kontraktturi = KontraktTuri::where('id', $id)->first();
        $header = '<h5 class="modal-title" id="createActionLabel">Qo\'shish</h5>';
        $footer = '<button type="button" class="btn btn-sm btn-white"
                                data-dismiss="modal">Yopish</button>
                        <a href="#" onclick="updateKontraktTuri('.$id.')" class="btn btn-sm btn-success">Saqlash</a>';
        $view = view('kontraktturi.edit',compact('kontraktturi'));
        return ['header' => $header, 'content' => $view->render(), 'footer' => $footer];
    }

    public function update(Request $request, $id)
    {
        if($data = $this->validate($request,[
            'name' => 'required'
        ],[
            'name.required' => 'Nomi maydoni kiritilishi shart!'
        ])){
            $KontraktTuri = KontraktTuri::where('id',$id)->first();
            $KontraktTuri->name = $data['name'];
            $KontraktTuri->save();
            return ['message' => 'Talim shakli tahrirlandi'];
        }
    }

    public function delete($id)
    {
        $kontraktturi = KontraktTuri::where('id', $id)->first();
        $header = '<h5 class="modal-title" id="createActionLabel">Tasdiqlang</h5>';
        $footer = '<button type="button" class="btn btn-sm btn-white pull-left"
                                data-dismiss="modal">Yopish</button>
                        <a href="#" onclick="deleteKontraktTuri('.$kontraktturi->id.')"class="btn btn-sm btn-danger" style="margin: 2px;">
            Ha</a>';
        $content = '<h4>Rostdan ham o\'chirishni xohlaysizmi?</h4>';
        return ['header' => $header, 'content' => $content, 'footer' => $footer];
    }

    public function destroy($id)
    {
        $model = KontraktTuri::where('id', $id)->first();
        if ($model) {
            $model->delete();
        }
        return back()->withSuccess(__("message.data_deleted"));
    }

}
