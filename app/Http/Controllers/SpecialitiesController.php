<?php

namespace App\Http\Controllers;

use App\Http\Filters\SpecialityFilter;
use App\Models\Speciality;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

class SpecialitiesController extends Controller
{
    public function index(SpecialityFilter $filter)
    {
        $datas = Speciality::with('faculty')->filter($filter)->paginate(15);
        return  view('specialities.index',compact('datas'));
    }

    public function create()
    {
        $header = '<h5 class="modal-title" id="createActionLabel">Qo\'shish</h5>';
        $footer = '<button type="button" class="btn btn-sm btn-white"
                                data-dismiss="modal">Yopish</button>
                        <a href="#" id="createSubmit" class="btn btn-sm btn-success">Saqlash</a>';
        $view = view('specialities.create');
        return ['header' => $header, 'content' => $view->render(), 'footer' => $footer];
    }


    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'code' => 'required',
            'faculty_id' => 'required'
        ],[
            'name.required' => 'Nomi maydoni to\'ldirilishi shart',
            'code.required' => 'Shifr maydoni to\'ldirilishi shart',
            'faculty_id.required' => 'Fakultet maydoni to\'ldirilishi shart'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'error' => true,
                'message' => $validator->errors(),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $specialities = new Speciality();
        $specialities->name = $request->name;
        $specialities->code = $request->code;
        $specialities->faculty_id = $request->faculty_id;
        $specialities->save();
        return ['message' => 'Yo\'nalish shakli yaratildi'];
    }

    public function edit($id)
    {
        $speciality = Speciality::where('id', $id)->first();
        $header = '<h5 class="modal-title" id="createActionLabel">Qo\'shish</h5>';
        $footer = '<button type="button" class="btn btn-sm btn-white"
                                data-dismiss="modal">Yopish</button>
                        <a href="#" onclick="updateSpeciality('.$id.')" class="btn btn-sm btn-success">Saqlash</a>';
        $view = view('specialities.edit',compact('speciality'));
        return ['header' => $header, 'content' => $view->render(), 'footer' => $footer];
    }

    public function update(Request $request, $id)
    {
        if($data = $this->validate($request,[
            'name' => 'required',
            'code' => 'required',
            'faculty_id' => 'required'
        ],[
            'name.required' => 'Nomi maydoni kiritilishi shart!',
            'code.required' => 'Shifr maydoni kiritilishi shart!',
            'faculty_id.required' => 'Fakultet maydoni kiritilishi shart!'
        ])){
            $speciality = Speciality::where('id',$id)->first();
            $speciality->name = $data['name'];
            $speciality->code = $data['code'];
            $speciality->faculty_id = $data['faculty_id'];
            $speciality->save();
            return ['message' => 'Yo\'nalish shakli tahrirlandi'];
        }
    }

    public function delete($id)
    {
        $speciality = Speciality::where('id', $id)->first();
        $header = '<h5 class="modal-title" id="createActionLabel">Tasdiqlang</h5>';
        $footer = '<button type="button" class="btn btn-sm btn-white pull-left"
                                data-dismiss="modal">Yopish</button>
                        <a href="#" onclick="deleteSpeciality('.$speciality->id.')"class="btn btn-sm btn-danger" style="margin: 2px;">
            Ha</a>';
        $content = '<h4>Rostdan ham o\'chirishni xohlaysizmi?</h4>';
        return ['header' => $header, 'content' => $content, 'footer' => $footer];
    }

    public function destroy($id)
    {
        $model = Speciality::where('id', $id)->first();
        if ($model) {
            $model->delete();
        }
        return back()->withSuccess(__("message.data_deleted"));
    }

}
