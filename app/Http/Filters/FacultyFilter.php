<?php


namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;

class FacultyFilter extends QueryFilter
{
    /**
     * @param string $name
     */
    public function name(string $name){
        $this->builder->where(function (Builder $query) use ($name) {
            $query->where('name', 'like', "%$name%");
        });
    }

    /**
     * @param string $key
     */
    public function key(string $key){
        $this->builder->where(function (Builder $query) use ($key) {
            $query->where('key', 'like', "%$key%");
        });
    }


}
