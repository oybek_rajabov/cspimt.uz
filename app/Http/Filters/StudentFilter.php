<?php


namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;

class StudentFilter extends QueryFilter
{
    /**
     * @param string $fio
     */
    public function fio(string $fio){
        $this->builder->where(function (Builder $query) use ($fio) {
            $query->where('fio', 'like', "%$fio%");
        });
    }


    /**
     * @param string $faculty
     */
    public function faculty(string $faculty){
        $this->builder->where(function (Builder $query) use ($faculty) {
            $query->where('faculty_id', $faculty);
        });
    }


    /**
     * @param string $speciality
     */
    public function speciality(string $speciality){
        $this->builder->where(function (Builder $query) use ($speciality) {
            $query->where('yonalish_id', $speciality);
        });
    }

    /**
     * @param string $talimturi
     */
    public function talimturi(string $talimturi){
        $this->builder->where(function (Builder $query) use ($talimturi) {
            $query->where('talim_turi_id', $talimturi);
        });
    }

    /**
     * @param string $course
     */
    public function course(string $course){
        $this->builder->where(function (Builder $query) use ($course) {
            $query->where('kursi', $course);
        });
    }

}
