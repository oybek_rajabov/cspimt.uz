<?php


namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;

class YonalishTalimTuriFilter extends QueryFilter
{
    /**
     * @param string $name
     */
    public function talimturiId(string $talimturiId){
        $this->builder->where(function (Builder $query) use ($talimturiId) {
            $query->where('talim_turi_id', '=', $talimturiId);
        });
    }

    /**
     * @param string $key
     */
    public function specialityId(string $specialityId){
        $this->builder->where(function (Builder $query) use ($specialityId) {
            $query->where('yonalish_id', '=', $specialityId);
        });
    }


}
