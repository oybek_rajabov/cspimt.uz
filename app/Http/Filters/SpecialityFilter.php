<?php


namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;

class SpecialityFilter extends QueryFilter
{
    /**
     * @param string $name
     */
    public function name(string $name){
        $this->builder->where(function (Builder $query) use ($name) {
            $query->where('name', '=', $name);
        });
    }

    public function code(string $code){
        $this->builder->where(function (Builder $query) use ($code) {
            $query->where('code', '=', $code);
        });
    }


    public function facultyId(string $facultyId){
        $this->builder->where(function (Builder $query) use ($facultyId) {
            $query->where('faculty_id', $facultyId);
        });
    }

}
