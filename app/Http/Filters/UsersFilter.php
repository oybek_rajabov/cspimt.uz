<?php


namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;

class UsersFilter extends QueryFilter
{

    protected $role_id;
    /**
     * @param string $username
     */
    public function role(string $role){
        $this->builder->where(function (Builder $query) use ($role) {
            $this->role_id = $role;
            $query->with('role')
            ->whereHas('role', function($q){
                $q->where('id',$this->role_id);
            });
        });
    }

    /**
     * @param string $username
     */
    public function username(string $username){
        $this->builder->where(function (Builder $query) use ($username) {
            $query->where('username', 'like', "%$username%");
        });
    }

    /**
     * @param string $email
     */
    public function fio(string $fio){
        $this->builder->where(function (Builder $query) use ($fio) {
            $query->where('fio', 'like', "%$fio%");
        });
    }

    /**
     * @param string $phone
     */
    public function phone(string $phone){
        $this->builder->where(function (Builder $query) use ($phone) {
            $query->where('phone', 'like', "%$phone%");
        });
    }
//
//
//    /**
//     * @param string $status
//     */
//    public function status(string $status){
//        $this->builder->where(function (Builder $query) use ($status) {
//            $query->where('status', $status);
//        });
//    }
//
//
//    public function datecr(string $datecr){
//        $this->builder->where(function (Builder $query) use ($datecr) {
//            $query->whereBetween('date_cr', [$datecr. ' 00:00:00' , $datecr. ' 23:59:59']);
//        });
//    }

}
