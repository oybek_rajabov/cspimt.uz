<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;

class SetLocale
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, \Closure $next)
    {
        $this->setLang($request);
        return $next($request);
    }

    protected function setLang($request)
    {
        if(isset($request->lang) && ($request->lang != 'undefined' )) {
            app()->setLocale($request->lang);
        }
    }
}
