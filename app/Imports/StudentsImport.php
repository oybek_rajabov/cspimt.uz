<?php

namespace App\Imports;

use App\Models\Student;
use Maatwebsite\Excel\Concerns\ToModel;

class StudentsImport implements ToModel
{

    public function model(array $row)
    {
        return new Student([
            'shart_n'     => $row[0],
            'fio'     => $row[1],
            'p_seria'     => $row[2],
            'p_number'     => $row[3],
            'address'     => $row[4],
            'talim_turi_id'     => $row[5],
            'talim_tili_id'     => $row[7],
            'kontrakt_turi_id'     => $row[8],
            'ksumma'     => $row[9],
            'stipendiya_t' => intval($row[10]),
            'kursi'     => $row[11],
            'a_id'     => $row[12],
            'telefon'  => $row[13]
        ]);
    }
}
