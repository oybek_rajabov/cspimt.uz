<?php
use App\Models\Language;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;


function admin(){
    if(\Illuminate\Support\Facades\Auth::check() && auth()->user()->getRoleName() != 'admin'){
        return true;
    }
    return false;
}


/**
 * @param $field_name
 * @param $files
 * @param $folder
 * @param $model
 * save files
 */
function saveFiles($field_name, $files, $folder, $model)
{
//    if(storage_path().'app/public/'.$folder)
//    if(is_dir(storage_path().'app/public/'.$folder)) {
//        mkdir($backupLoc, 0755, true);
//    }
    if (is_array($files))
        foreach ($files as $file) {
            $file_name = time() .$model->id. $file->getClientOriginalName();
            $file->storeAs('public/' . $folder, $file_name);
            $newFile = new Files();
            $newFile->table_name = $model->getTable();
            $newFile->field_id = $model->id;
            $newFile->field_name = $field_name;
            $newFile->file_name = $file_name;
            $newFile->save();
        }
    if (!is_array($files) && $files != null) {
        $file_name = time() .$model->id. $files->getClientOriginalName();
        $files->storeAs('public/' . $folder, $file_name);
        $newFile = new Files();
        $newFile->table_name = $model->getTable();
        $newFile->field_id = $model->id;
        $newFile->field_name = $field_name;
        $newFile->file_name = $file_name;
        $newFile->save();
    }
}


/**
 * delete file
 */
function delete($folder, $fileName)
{
    return true;
}

function actionView($url)
{
    echo '<a href=' . $url . ' title="Ko\'rish" class="btn btn-success btn-xs" style="margin: 2px;">
    <span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;';
}
function actionModalView($id)
{
    echo '<a title="Ko\'rish" onclick="showAction('.$id.')" class="btn btn-success btn-xs" style="margin: 2px;">
    <span class="glyphicon glyphicon-eye-open"></span></a>&nbsp;';
}

function actionEdit($url)
{
    echo '<a href=' . $url . ' title="Tahrirlash" class="btn btn-warning btn-xs" style="margin: 2px;">
    <span class="glyphicon glyphicon-pencil"></span></a>&nbsp;';
}

function actionModalEdit($id)
{
    echo '<a onclick="showUpdateModal('.$id.')" title="Tahrirlash" class="btn btn-success btn-xs" style="margin: 2px;">
    <span class="glyphicon glyphicon-pencil"></span></a>&nbsp;';
}

function actionDelete($model)
{
//    $tableName = $model->getTable();
    echo '<a href="#" id="delete{$model->id}" onclick="deleteModel('.$model->id.')"
            data-id="{$model->id}" title="O\'chirish" class="btn btn-danger btn-xs" style="margin: 2px;">
                                    <span style="font-size: 12px;" class="glyphicon glyphicon-trash"></span>
            </a>';
}
function actionModalDelete($id)
{
    echo '<a onclick="showDeleteModal('.$id.')" title="O\'chirish" class="btn btn-danger btn-xs" style="margin: 2px;">
    <span class="glyphicon glyphicon-trash"></span></a>&nbsp;';
}
/**
 * @param $image
 * @param $image_folder
 * @return string
 * get image
 */
function getImage($image , $image_folder){
    $path = storage_path().'/app/public';
    if ($image && file_exists($path.'/'.$image_folder.'/'.$image)) {
        return  config('app.site_link'). '/storage/'.$image_folder.'/'.$image;
    }
    return config('app.site_link'). '/img/noimg.jpg';
}

/**
 * @param $date
 * @param int $type
 * @return false|string
 * get date
 */
function  dateFormatter($date ,$type = 2)
{
    return $type == 2 ?   date('Y-m-d H:i', strtotime($date)) : date('Y-m-d', strtotime($date));
}


/**
 * @param $text
 * @return array|string|string[]
 */
function replaceWorld($text){
    $text = str_replace('-','',$text);
    $text = str_replace(' ','',$text);
    return $text;
}


/**
 * @param $file
 * @param $id
 * @param $image_folder
 * @param null $old_image_name
 * @return string
 */
function fileSaved($file , $id , $image_folder , $old_image_name = null){
    $fileName = 'photo_' . $id . time() . '_' . $file->getClientOriginalName();
    $path = storage_path().'/app/public';
    if ($old_image_name && file_exists($path.'/'.$image_folder.'/'.$old_image_name)) {
        unlink($path.'/'.$image_folder.'/'.$old_image_name);
    }
    File::isDirectory($path.'/'.$image_folder) or File::makeDirectory($path.'/'.$image_folder, 0777, true, true);
    $file->storeAs('/public/'. $image_folder,$fileName );
    return $fileName;
}





 function file_get_contents_curl($url){
    $arrContextOptions = stream_context_create([
        "ssl"=>[
            "verify_peer"   =>  false,
            "verify_peer_name"  =>  false,
        ],
    ]);
    return file_get_contents($url , false , $arrContextOptions);
}

function faculties(){
    $faculties = \App\Models\Faculty::all();
    return $faculties ?? [];
}
function talimturlari(){
    $talimturlari = \App\Models\TalimTuri::all();
    return $talimturlari ?? [];
}

function specialities(){
    $specialities = \App\Models\Speciality::all();
    return $specialities ?? [];
}

