<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    use HasFactory;
    protected $table = 'languages';
    protected $fillable = ['url','local','name','default','status'];
    public $timestamps = false;

    public static function getActiveLanguages(){
        return self::where('status' , 1)->get();
    }


    public static function getDefault(){
        return self::where('default',1)->first();
    }
}
