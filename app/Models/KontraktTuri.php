<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KontraktTuri extends Model
{
    use HasFactory;
    protected $table = 'kontrakt_turi';
    public $timestamps = false;
}
