<?php

namespace App\Models;

use App\Http\Filters\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Speciality extends Model
{
    use HasFactory, Filterable, Sortable;
    protected $table = 'yonalish';
    public $timestamps = false;

    public function faculty()
    {
        return $this->hasOne(Faculty::class,'id','faculty_id');
    }
}
