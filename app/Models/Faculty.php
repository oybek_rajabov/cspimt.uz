<?php

namespace App\Models;

use App\Http\Filters\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Faculty extends Model
{
    use HasFactory, Sortable, Filterable;
    public $timestamps = false;
    protected $table = 'faculties';

    protected $fillable = [
        'id',
        'name',
        'key',
    ];

}
