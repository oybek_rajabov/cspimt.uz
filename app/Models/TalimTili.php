<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TalimTili extends Model
{
    use HasFactory;
    protected $table = 'talim_tili';
    public $timestamps = false;
}
