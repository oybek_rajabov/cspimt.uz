<?php

namespace App\Models;

use App\Http\Filters\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Student extends Model
{
    use HasFactory, Sortable, Filterable;
    public $timestamps = false;
    protected $table = 'students';
    const SAVE_FOLDER_IMG = 'students';
    public $filterable = [
        'fio',
        'faculty',
        'speciality.name',
        'course'
    ];
    protected $fillable = [
        'id',	// bigint Автоматическое приращение [nextval('students_id_seq')]
        'fio',	// character varying(255) NULL
        'telefon',	// character varying(255) NULL
        'email',	// character varying(255) NULL
        'image',	// character varying(255) NULL
        'p_seria',	// character varying(255) NULL
        'p_number',	// character varying(255) NULL
        'jins',	// character varying(255) NULL
        'kursi',	// character varying(255) NULL
        'nationality',	// character varying(255) NULL
        'address',	// character varying(255) NULL
        'shart_n',	// character varying(255) NULL
        't_turi',	// character varying(255) NULL
        'st_tayinlash',	// character varying(255) NULL
        'stipendiya_t',	// character varying(255) NULL
        'ksumma',	// character varying(255) NULL
        'yangi_tolov',	// character varying(255) NULL
        'tolandi',	// character varying(255) NULL
        'umumiy_tolov',	// character varying(255) NULL
        'qarzdorlik',	// character varying(255) NULL
        'ortgan_pul',	// character varying(255) NULL
        'shartnoma',	// character varying(255) NULL
        'shartnoma_date',	// character varying(255) NULL
        'talim_tili_id',	// character varying(255) NULL
        'kontrakt_turi_id',	// character varying(255) NULL
        'talim_turi_id',	// character varying(255) NULL
        'yonalish_id',	// character varying(255) NULL
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     * speciality modeli bilan bog'lanish
     */
    public function speciality()
    {
        return $this->hasOne(Speciality::class,'id','yonalish_id');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     * speciality modeli bilan bog'lanish
     */
    public function faculty()
    {
        return $this->hasOne(Faculty::class,'id','faculty_id');
    }

    public function talimturi()
    {
        return $this->hasOne(TalimTuri::class,'id','talim_turi_id');
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function getSpeciality($id){
        return Speciality::where('faculty_id', $id)->get();
    }

    /**
     * @return string
     */
    public function getImage(){
        return getImage($this->image, Student::SAVE_FOLDER_IMG);
    }


    public function saveData($data)
    {
        $this->fio = $data['fio'] ?? null;
        $this->nationality = replaceWorld($data['nationality']) ?? '';
        $this->email = replaceWorld($data['email']) ?? '';
        $this->telefon = replaceWorld($data['telefon']) ?? '';
        $this->p_seria = replaceWorld($data['p_seria']) ?? '';
        $this->p_number = replaceWorld($data['p_number']) ?? '';
        $this->jins = $data['jins'] ? ($data['jins'] ? 1:0) : null;
        $this->kursi = $data['course'] ?? 0;
        $this->address = $data['address'] ?? '';
        $this->yonalish_id = $data['speciality_id'] ?? null;
        $this->faculty_id = $data['faculty_id'] ?? null;
        $this->talim_turi_id = $data['talim_turi_id'] ?? null;
        $this->talim_tili_id = $data['talim_tili_id'] ?? null;
        $this->kontrakt_turi_id = $data['kontrakt_turi_id'] ?? null;
        $this->save();
        return true;
    }
}
