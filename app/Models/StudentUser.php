<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class StudentUser extends Authenticatable
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'students';
}
