<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TalimTuri extends Model
{
    use HasFactory;
    protected $table = 'talim_turi';
    public $timestamps = false;
}
