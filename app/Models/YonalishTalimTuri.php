<?php

namespace App\Models;

use App\Http\Filters\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class YonalishTalimTuri extends Model
{
    use HasFactory, Filterable, Sortable;
    protected $table = 'yonalish_talim_turi';
    public $timestamps = false;


    public function speciality()
    {
        return $this->hasOne(Speciality::class,'id','yonalish_id');
    }

    public function talimturi()
    {
        return $this->hasOne(TalimTuri::class,'id','talim_turi_id');
    }
}
