<?php

namespace App\Traits;

trait Additional
{

    /**
     * @param $translate
     * @param $field
     * @param int $type
     * @return mixed|null
     * type = 1 object
     * type = 2 array
     */
    public static function getTranslate($translate , $field , $type = 1)
    {
        if(!$translate) return null;
        if($type == 1)
        {
            # for object ( type = 1)
            foreach ($translate as $value){
                if($value->field_name == $field) return $value->field_value;
            }
            return null;
        }

        # for array ( type = 2 )
        foreach ($translate as $value){
            if($value['field_name'] == $field) return $value['field_value'];
        }
        return null;
    }

    public static function getTranslateDef($translate , $field , $type = 1 , $default)
    {
        if(!$translate) return $default;
        if($type == 1)
        {
            # for object ( type = 1)
            foreach ($translate as $value){
                if($value->field_name == $field && $value->field_value) return $value->field_value;
            }
            return $default;
        }

        # for array ( type = 2 )
        foreach ($translate as $value){
            if($value['field_name'] == $field) return $value['field_value'];
        }
        return $default;
    }

}
