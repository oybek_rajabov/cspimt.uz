<?php

namespace App\Traits;

trait ApiResponse
{

    /**
     * @param $data
     * @param null $message
     * @param int $code
     * success method
     */
    protected function successResponse($data, $message = null, int $code = 200)
    {
        $result = [
            'errors'  => false,
            'message' => $message,
            'data'    => $data
        ];
        return response($result , $code);
    }


    /**
     * @param null $message
     * @param int $code
     */
    protected function errorResponse($message = null, int $code = 422)
    {
        $result = [
            'errors'  => true,
            'message' => $message,
            'data'    => null
        ];
        return response($result, $code);
    }

}
