<?php


use App\Http\Controllers\HomeController;
use App\Http\Controllers\FacultiesController;
use App\Http\Controllers\KontrakTuriController;
use App\Http\Controllers\SpecialitiesController;
use App\Http\Controllers\StudentsController;
use App\Http\Controllers\StudentUsersController;
use App\Http\Controllers\TalimTiliController;
use App\Http\Controllers\TalimTuriController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\WorkController;
use App\Http\Controllers\YonalishTalimTuriController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

    Route::get('students/login', [StudentUsersController::class, 'login'])->name('student-user.login');
    Route::post('students/login', [StudentUsersController::class, 'handleLogin'])->name('student-user.handleLogin');

    Route::group(['prefix' => 'students', 'middleware' => ['auth:webstudent']], function ($router) {
        Route::get('', [StudentUsersController::class, 'index'])->name('student-user.home');
        Route::get('/logout', [StudentUsersController::class, 'index'])->name('student-user.logout');
    });
Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::get('/home', [HomeController::class, 'index'])->name('home');


    Route::get('users/profile/show/{user}', [UsersController::class, 'profileShow'])->name('users.profile.show');
    Route::post('users/profile/update', [UsersController::class, 'profileUpdate'])->name('users.profile.update');


    /////////////////////////////Prorektor///////////////////////////////////////////
    Route::group(['middleware' => 'admin'], function () {
        Route::prefix('admin')->group(function () {
            Route::resource('users', UsersController::class);
            Route::match(['GET', 'DELETE'], 'users/delete/{user}', [UsersController::class, 'delete'])->name('users.delete');

            /*******************************  Fakultetlar ************************************/
            Route::group(['prefix' => 'faculties'], function () {
                Route::get('/index', [FacultiesController::class, 'index'])->name('admin.faculties.index');
                Route::get('/create', [FacultiesController::class, 'create'])->name('admin.faculties.create');
                Route::post('/store', [FacultiesController::class, 'store'])->name('admin.faculties.store');
                Route::get('/edit/{id}', [FacultiesController::class, 'edit'])->name('admin.faculties.edit');
                Route::post('/update/{id}', [FacultiesController::class, 'update'])->name('admin.faculties.update');
                Route::get('/delete/{id}', [FacultiesController::class, 'delete'])->name('admin.faculties.delete');
                Route::post('/destroy/{id}', [FacultiesController::class, 'destroy'])->name('admin.faculties.destroy');
            });

            /*******************************  Talim shakllari ************************************/
            Route::group(['prefix' => 'talimturi'], function () {
                Route::get('/index', [TalimTuriController::class, 'index'])->name('admin.talimturi.index');
                Route::get('/create', [TalimTuriController::class, 'create'])->name('admin.talimturi.create');
                Route::post('/store', [TalimTuriController::class, 'store'])->name('admin.talimturi.store');
                Route::get('/edit/{id}', [TalimTuriController::class, 'edit'])->name('admin.talimturi.edit');
                Route::post('/update/{id}', [TalimTuriController::class, 'update'])->name('admin.talimturi.update');
                Route::get('/delete/{id}', [TalimTuriController::class, 'delete'])->name('admin.talimturi.delete');
                Route::post('/destroy/{id}', [TalimTuriController::class, 'destroy'])->name('admin.talimturi.destroy');
            });

            /*******************************  Shartnoma turlari ************************************/
            Route::group(['prefix' => 'kontraktturi'], function () {
                Route::get('/index', [KontrakTuriController::class, 'index'])->name('admin.kontraktturi.index');
                Route::get('/create', [KontrakTuriController::class, 'create'])->name('admin.kontraktturi.create');
                Route::post('/store', [KontrakTuriController::class, 'store'])->name('admin.kontraktturi.store');
                Route::get('/edit/{id}', [KontrakTuriController::class, 'edit'])->name('admin.kontraktturi.edit');
                Route::post('/update/{id}', [KontrakTuriController::class, 'update'])->name('admin.kontraktturi.update');
                Route::get('/delete/{id}', [KontrakTuriController::class, 'delete'])->name('admin.kontraktturi.delete');
                Route::post('/destroy/{id}', [KontrakTuriController::class, 'destroy'])->name('admin.kontraktturi.destroy');
            });

            /*******************************  Talim tillari ************************************/
            Route::group(['prefix' => 'talimtili'], function () {
                Route::get('/index', [TalimTiliController::class, 'index'])->name('admin.talimtili.index');
                Route::get('/create', [TalimTiliController::class, 'create'])->name('admin.talimtili.create');
                Route::post('/store', [TalimTiliController::class, 'store'])->name('admin.talimtili.store');
                Route::get('/edit/{id}', [TalimTiliController::class, 'edit'])->name('admin.talimtili.edit');
                Route::post('/update/{id}', [TalimTiliController::class, 'update'])->name('admin.talimtili.update');
                Route::get('/delete/{id}', [TalimTiliController::class, 'delete'])->name('admin.talimtili.delete');
                Route::post('/destroy/{id}', [TalimTiliController::class, 'destroy'])->name('admin.talimtili.destroy');
            });

            /*******************************  Yo'nalishlar ************************************/
            Route::group(['prefix' => 'specialities'], function () {
                Route::get('/index', [SpecialitiesController::class, 'index'])->name('admin.specialities.index');
                Route::get('/create', [SpecialitiesController::class, 'create'])->name('admin.specialities.create');
                Route::post('/store', [SpecialitiesController::class, 'store'])->name('admin.specialities.store');
                Route::get('/edit/{id}', [SpecialitiesController::class, 'edit'])->name('admin.specialities.edit');
                Route::post('/update/{id}', [SpecialitiesController::class, 'update'])->name('admin.specialities.update');
                Route::get('/delete/{id}', [SpecialitiesController::class, 'delete'])->name('admin.specialities.delete');
                Route::post('/destroy/{id}', [SpecialitiesController::class, 'destroy'])->name('admin.specialities.destroy');
            });

            /*******************************  Yo'nalish talim tui ************************************/
            Route::group(['prefix' => 'yonalishtalimturi'], function () {
                Route::get('/index', [YonalishTalimTuriController::class, 'index'])->name('admin.yonalishtalimturi.index');
                Route::get('/create', [YonalishTalimTuriController::class, 'create'])->name('admin.yonalishtalimturi.create');
                Route::post('/store', [YonalishTalimTuriController::class, 'store'])->name('admin.yonalishtalimturi.store');
                Route::get('/edit/{id}', [YonalishTalimTuriController::class, 'edit'])->name('admin.yonalishtalimturi.edit');
                Route::post('/update/{id}', [YonalishTalimTuriController::class, 'update'])->name('admin.yonalishtalimturi.update');
                Route::get('/delete/{id}', [YonalishTalimTuriController::class, 'delete'])->name('admin.yonalishtalimturi.delete');
                Route::post('/destroy/{id}', [YonalishTalimTuriController::class, 'destroy'])->name('admin.yonalishtalimturi.destroy');
            });

        });
    });
    /////////////////////////////Prorektor///////////////////////////////////////////
    Route::group(['middleware' => 'marketing'], function () {

    });


    /////////////////////////////Tasks///////////////////////////////////////////

    ////;/////////////////////////marketing_xodim///////////////////////////////////////////
    Route::group(['middleware' => 'marketing_xodim'], function () {
        Route::prefix('marketing_xodim')->group(function () {
            /***************************** Student  *****************************************************/
            Route::group(['prefix' => 'students'], function () {
                Route::get('/index', [StudentsController::class, 'index'])->name('students.index');
                Route::get('/create', [StudentsController::class, 'create'])->name('students.create');
                Route::post('/store', [StudentsController::class, 'store'])->name('students.store');
                Route::get('/edit/{model}', [StudentsController::class, 'edit'])->name('students.edit');
                Route::post('/update/{model}', [StudentsController::class, 'update'])->name('students.update');
                Route::post('/delete/{student}', [StudentsController::class, 'delete'])->name('students.delete');
                Route::match(['GET', 'DELETE'], '/delete/{student}', [StudentsController::class, 'delete'])->name('students.delete');
                Route::get('/specialty', [StudentsController::class, 'getSpecialty'])->name('students.specialty');
                Route::get('/group-list', [StudentsController::class, 'getGroupList'])->name('students.group');
                Route::post('/import-students', [StudentsController::class, 'importStudents'])->name('students.import-students');
                Route::get('/download-excel-template', [StudentsController::class, 'downloadExcelTemplate'])->name('students.download-excel-template');
                Route::post('/delete-all', [StudentsController::class, 'deleteAll'])->name('students.delete-all');
                Route::post('/shartnoma-turi', [StudentsController::class, 'shartnomaTuri'])->name('students.shartnoma-turi');
                Route::post('/shartnomaga', [StudentsController::class, 'shartnomaga'])->name('students.shartnomaga');

            });

        });
    });
});
