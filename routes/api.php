<?php

use App\Http\Controllers\Api\Auth\LoginController;
use App\Http\Controllers\Api\Student\StudentController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

    /**
     ********************* auth *********************************
     */
    Route::group(['prefix' => 'auth/'], function () {
        Route::post('login', [LoginController::class, 'login']);
    });


    Route::group(['middleware' => 'auth:sanctum'], function () {

        Route::group(['prefix' => 'student'], function () {
            Route::post('get-student/', [StudentController::class, 'getModel']);
        });

    });







